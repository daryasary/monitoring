from django.db.models.signals import post_save
from django.dispatch import receiver

from datetime import datetime

from .models import Alert
from .tasks import start_alert


@receiver(post_save, sender=Alert)
def alert_callback(sender, instance, created, *args, **kwargs):
    if created:
        first_run = datetime.utcnow() + instance.duration
        # start_alert.apply_async((instance.id, ), eta=first_run)
