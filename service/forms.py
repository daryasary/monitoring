from django import forms

from users.models import User


class AddRemoveUserOfReportsForm(forms.Form):
    user = forms.ChoiceField(widget=forms.Select)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        self.form_name = kwargs.pop('form_name')
        super().__init__(*args, **kwargs)
        queryset = User.objects.filter(is_active=True).exclude(is_superuser=True)
        self.fields['user'].choices = [(qs.id, qs.username) for qs in queryset]
