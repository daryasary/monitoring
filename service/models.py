from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class Service(models.Model):
    title = models.CharField(_('Title'), max_length=50, unique=True)
    description = models.TextField(_('Description'), blank=True)
    url = models.URLField(_('Service URL'), max_length=150, default='', blank=True)
    token = models.CharField(_('Token'), max_length=250, blank=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    created_time = models.DateTimeField(_('Created Time'), auto_now_add=True)
    modified_time = models.DateTimeField(_('Modified Time'), auto_now=True)

    class Meta:
        db_table = 'services'
        app_label = 'service'
        ordering = ('title',)

    def __str__(self):
        return self.title


class ReportLiveManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_enable=True)


class Report(models.Model):
    title = models.CharField(_('Title'), max_length=250, db_index=True)
    description = models.TextField(_('Description'), blank=True)
    slug = models.SlugField(_('Report Slug'), max_length=50, db_index=True)
    service = models.ForeignKey(Service, related_name='report_service')
    url = models.URLField(_('Report URL'), max_length=500, default='', blank=True)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL)
    is_enable = models.BooleanField(_('Is Enable'), default=True)
    has_save = models.BooleanField(_('Has Save'), default=True)
    token = models.CharField(_('Token'), max_length=250, blank=True)
    created_time = models.DateTimeField(_('Created Time'), auto_now_add=True)
    modified_time = models.DateTimeField(_('Modified Time'), auto_now=True)

    objects = models.Manager()
    live = ReportLiveManager()

    class Meta:
        db_table = 'reports'
        app_label = 'service'

    def __str__(self):
        return '%s-%s' % (self.service.title, self.title)

    def user_access(self, user):
        if user.is_superuser:
            return True
        return user in self.users.all()


class AlertUserProfile(models.Model):
    SENDING_METHOD = (
        ('sms', _('SMS')),
        # ('email', _('EMAIL')),
        # ('push', _('PUSH')),
    )

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='alert_user_profile')
    message_sending_method = models.CharField(_('Message Sending Method'), max_length=50, choices=SENDING_METHOD)

    class Meta:
        db_table = 'alert_user_profile'
        app_label = 'service'

    def __str__(self):
        return self.user.username


class Alert(models.Model):
    title = models.CharField(_('Title'), max_length=50)
    description = models.TextField(_('Description'), blank=True)
    url = models.URLField(_('Alert URL'), max_length=255)
    token = models.CharField(_('Token'), max_length=250, blank=True)
    duration = models.DurationField(_('Duration'), null=False, blank=False)
    is_enable = models.BooleanField(_('Is Enable'), default=True)
    created_time = models.DateTimeField(_('Created Time'), auto_now_add=True)
    modified_time = models.DateTimeField(_('Modified Time'), auto_now=True)

    class Meta:
        db_table = 'alert'
        app_label = 'service'

    def __str__(self):
        return self.title


class AlertValue(models.Model):
    OPERATOR_CHOICE = (
        ('>', '>'),
        ('>=', '>='),
        ('<', '<'),
        ('<=', '<='),
        ('==', '=='),
        ('!=', '!=')
    )

    alert = models.ForeignKey(Alert)
    key = models.CharField(_('Key'), max_length=50)
    value_operator = models.CharField(_('Value Operator'), max_length=10, choices=OPERATOR_CHOICE)
    value = models.FloatField(_('Value'))
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, help_text=_('Users that will be informed by this alert.'))
    message = models.TextField(_('Message'), blank=True)

    class Meta:
        db_table = 'alert_value'
        app_label = 'service'

    def __str__(self):
        return self.alert.title


class AlertSendMessageLog(models.Model):
    alert_value = models.ForeignKey(AlertValue)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    send_method = models.CharField(_('Sending Method'), max_length=50, null=True, blank=True)
    send_status = models.BooleanField(_('Is Sent'))
    send_time = models.DateTimeField(_('Sent Time'), auto_now_add=True)
    description = models.TextField(_('Description'), blank=True)

    class Meta:
        db_table = 'alert_send_log'
        app_label = 'service'


class SMSSendLog(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    message = models.TextField(_('Message Text'), blank=True)
    phone_number = models.CharField(_('Phone Number'), max_length=20)
    send_status = models.BooleanField(_('Is Sent'))
    send_time = models.DateTimeField(_('Sent Time'), auto_now_add=True)
    description = models.TextField(_('Description'), blank=True)
    # operator = models.CharField(_('Operator'), max_length=50)

    class Meta:
        db_table = 'sms_send_log'
        app_label = 'service'
