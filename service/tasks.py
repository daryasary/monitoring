from django.template.loader import render_to_string

from celery import shared_task
from datetime import datetime
import requests

from utils.send_message import adp_send
from .models import Alert, AlertUserProfile, AlertSendMessageLog


# TODO: modify tasks...
# TODO: add logger...

# Check Alert, get value from defined url, check data and send message if evaluation is true ==========================
def check_alert(alert_id):

    try:
        alert = Alert.objects.get(id=alert_id, is_enable=True)
    except Alert.DoesNotExist:
        return False

    try:
        headers = {'Authorization': 'Token %s' % alert.token}
        response = requests.get(alert.url, headers=headers).json()
    except:
        response = {}

    if response:
        for alert_value in alert.alertvalue_set.all():
            response_value = response.get(alert_value.key)
            if response_value:
                if eval('%s%s%s' % (float(response_value), alert_value.value_operator, alert_value.value)):
                    users = alert_value.users.values('id', 'username')
                    for user in users:
                        alert_data = {'alert_value_id': alert_value.id,
                                      'user_id': user['id'],
                                      'message': alert_value.message,
                                      'alert_title': alert.title,
                                      'alert_value': '%s%s%s' % (float(response_value), alert_value.value_operator, alert_value.value)
                                      }
                        send_message.apply_async((alert_data, ), eta=datetime.utcnow())

    next_run = datetime.utcnow() + alert.duration
    start_alert.apply_async((alert.id, ), eta=next_run)


# Start of Alert Task (Call by post_save signal and recall by check_alert ) ===========================================
# @shared_task(bind=True)
def start_alert(self, alert_id):

    check_alert(alert_id)

    return True


# Send Message Task (Called if evaluation was true) ===================================================================
# @shared_task(bind=True)
def send_message(self, alert_data):

    description = ''
    send_method = ''
    send_status = False

    try:
        alert_user_profile = AlertUserProfile.objects.get(user__id=alert_data['user_id'])
    except AlertUserProfile.DoesNotExist:
        description = 'User has not Alert Profile.'
    else:
        send_method = alert_user_profile.message_sending_method

        # Send Sms ------------------------------------------------------------
        if send_method == 'sms':
            phone_number = alert_user_profile.user.phone_number
            if phone_number:
                template_context = {'alert_title': alert_data['alert_title'],
                                    'alert_value': alert_data['alert_value'],
                                    'send_time': str(datetime.now()).split('.')[0]}

                message = render_to_string("alert_sms.txt", context=template_context)
                sms_status, sms_msg = adp_send(phone_number, message)

                if sms_status:
                    send_status = True
                    description = 'Sms sent. %s' % sms_msg
                else:
                    description = sms_msg

            else:
                description = 'User has not phone number.'

        # Send Email ----------------------------------------------------------
        # elif send_method == 'email':
        #     user_email = alert_user_profile.user.email
        #     if user_email:
        #         try:
        #             send_email()
        #         except Exception as e:
        #             description = 'Email not sent. Error: %s' % str(e)
        #         else:
        #             description = 'Sent'
        #             send_status = True
        #
        #     else:
        #         description = 'User has not email.'

    # Save Log ------------------------------------------------------
    AlertSendMessageLog(alert_value_id=alert_data['alert_value_id'],
                        user_id=alert_data['user_id'],
                        send_method=send_method,
                        send_status=send_status,
                        description=description
                        ).save()

    return True

