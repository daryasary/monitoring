# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0014_auto_20170422_1323'),
    ]

    operations = [
        migrations.AddField(
            model_name='report',
            name='has_save',
            field=models.BooleanField(default=True, verbose_name='Has Save'),
        ),
        migrations.AlterField(
            model_name='alert',
            name='token',
            field=models.CharField(blank=True, max_length=250, verbose_name='Token'),
        ),
        migrations.AlterField(
            model_name='report',
            name='title',
            field=models.CharField(max_length=50, db_index=True, verbose_name='Title'),
        ),
        migrations.AlterField(
            model_name='service',
            name='token',
            field=models.CharField(blank=True, max_length=250, verbose_name='Token'),
        ),
    ]
