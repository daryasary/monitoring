# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0006_auto_20170121_1226'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='service',
            name='password',
        ),
        migrations.RemoveField(
            model_name='service',
            name='username',
        ),
        migrations.AddField(
            model_name='report',
            name='password',
            field=models.CharField(blank=True, max_length=150),
        ),
        migrations.AddField(
            model_name='report',
            name='username',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
