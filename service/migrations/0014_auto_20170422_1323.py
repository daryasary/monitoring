# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('service', '0013_alerturl_alertuserprofile_alertvalue'),
    ]

    operations = [
        migrations.CreateModel(
            name='Alert',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('url', models.URLField(verbose_name='Alert URL', max_length=255)),
                ('token', models.CharField(blank=True, max_length=250)),
                ('duration', models.DurationField(verbose_name='Duration')),
                ('is_enable', models.BooleanField(verbose_name='Is Enable', default=True)),
                ('created_time', models.DateTimeField(verbose_name='Created Time', auto_now_add=True)),
                ('modified_time', models.DateTimeField(verbose_name='Modified Time', auto_now=True)),
            ],
            options={
                'db_table': 'alert',
            },
        ),
        migrations.CreateModel(
            name='AlertSendMessageLog',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('send_method', models.CharField(verbose_name='Sending Method', blank=True, null=True, max_length=50)),
                ('send_status', models.BooleanField(verbose_name='Is Sent')),
                ('send_time', models.DateTimeField(verbose_name='Sent Time', auto_now_add=True)),
                ('description', models.TextField(verbose_name='Description', blank=True)),
            ],
            options={
                'db_table': 'alert_send_log',
            },
        ),
        migrations.RemoveField(
            model_name='alertvalue',
            name='alert_url',
        ),
        migrations.AddField(
            model_name='alertvalue',
            name='message',
            field=models.TextField(verbose_name='Message', blank=True),
        ),
        migrations.AlterField(
            model_name='alertuserprofile',
            name='message_sending_method',
            field=models.CharField(choices=[('sms', 'SMS')], verbose_name='Message Sending Method', max_length=50),
        ),
        migrations.AlterField(
            model_name='alertuserprofile',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL, related_name='alert_user_profile'),
        ),
        migrations.AlterField(
            model_name='alertvalue',
            name='value',
            field=models.FloatField(verbose_name='Value'),
        ),
        migrations.AlterField(
            model_name='service',
            name='title',
            field=models.CharField(unique=True, verbose_name='Title', max_length=50),
        ),
        migrations.DeleteModel(
            name='AlertURL',
        ),
        migrations.AddField(
            model_name='alertsendmessagelog',
            name='alert_value',
            field=models.ForeignKey(to='service.AlertValue'),
        ),
        migrations.AddField(
            model_name='alertsendmessagelog',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='alertvalue',
            name='alert',
            field=models.ForeignKey(to='service.Alert', default=''),
            preserve_default=False,
        ),
    ]
