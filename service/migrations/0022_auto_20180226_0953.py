# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0021_report_token'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='service',
            options={'ordering': ('title',)},
        ),
    ]
