# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0007_auto_20170121_1318'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='url',
            field=models.URLField(blank=True, default='', verbose_name='Report URL', max_length=500),
        ),
    ]
