# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('service', '0015_auto_20170424_1623'),
    ]

    operations = [
        migrations.CreateModel(
            name='SMSSendLog',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('message', models.TextField(verbose_name='Message Text', blank=True)),
                ('phone_number', models.CharField(verbose_name='Phone Number', max_length=20)),
                ('send_status', models.BooleanField(verbose_name='Is Sent')),
                ('send_time', models.DateTimeField(verbose_name='Sent Time', auto_now_add=True)),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'sms_send_log',
            },
        ),
    ]
