# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0009_report_is_enable'),
    ]

    operations = [
        migrations.AddField(
            model_name='report',
            name='token',
            field=models.CharField(blank=True, max_length=250),
        ),
    ]
