# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0011_auto_20170218_0834'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='report',
            name='password',
        ),
        migrations.RemoveField(
            model_name='report',
            name='token',
        ),
        migrations.RemoveField(
            model_name='report',
            name='username',
        ),
        migrations.AddField(
            model_name='service',
            name='token',
            field=models.CharField(max_length=250, blank=True),
        ),
    ]
