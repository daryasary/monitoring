# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0005_auto_20170121_1201'),
    ]

    operations = [
        migrations.RenameField(
            model_name='service',
            old_name='user',
            new_name='username',
        ),
        migrations.AlterField(
            model_name='report',
            name='title',
            field=models.CharField(verbose_name='Title', max_length=50),
        ),
    ]
