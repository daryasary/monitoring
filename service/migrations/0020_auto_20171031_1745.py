# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0019_auto_20170517_1705'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='title',
            field=models.CharField(verbose_name='Title', max_length=250, db_index=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='url',
            field=models.URLField(verbose_name='Service URL', max_length=150, blank=True, default=''),
        ),
    ]
