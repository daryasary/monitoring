# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0002_auto_20170116_1301'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='slug',
            field=models.SlugField(verbose_name='Report Slug'),
        ),
    ]
