# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0008_auto_20170121_1320'),
    ]

    operations = [
        migrations.AddField(
            model_name='report',
            name='is_enable',
            field=models.BooleanField(verbose_name='Is Enable', default=True),
        ),
    ]
