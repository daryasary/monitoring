# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0010_report_token'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='created_time',
            field=models.DateTimeField(verbose_name='Created Time', auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='report',
            name='modified_time',
            field=models.DateTimeField(auto_now=True, verbose_name='Modified Time'),
        ),
        migrations.AlterField(
            model_name='report',
            name='password',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='service',
            name='created_time',
            field=models.DateTimeField(verbose_name='Created Time', auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='modified_time',
            field=models.DateTimeField(auto_now=True, verbose_name='Modified Time'),
        ),
    ]
