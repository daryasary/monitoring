# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='url',
            field=models.URLField(verbose_name='Report URL', max_length=50, blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='service',
            name='url',
            field=models.URLField(verbose_name='Service URL', max_length=50, blank=True, default=''),
        ),
    ]
