# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0020_auto_20171031_1745'),
    ]

    operations = [
        migrations.AddField(
            model_name='report',
            name='token',
            field=models.CharField(max_length=250, verbose_name='Token', blank=True),
        ),
    ]
