# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0003_auto_20170118_1147'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='password',
            field=models.CharField(max_length=150, blank=True),
        ),
        migrations.AddField(
            model_name='service',
            name='user',
            field=models.CharField(max_length=50, blank=True),
        ),
    ]
