# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('service', '0016_smssendlog'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='owner',
            field=models.ForeignKey(default=10, to=settings.AUTH_USER_MODEL),
        ),
    ]
