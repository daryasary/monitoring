# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(verbose_name='Name', max_length=50)),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('slug', models.SlugField(verbose_name='Report Slug', unique=True)),
                ('url', models.URLField(verbose_name='Report URL', default='', max_length=50)),
                ('created_time', models.DateTimeField(verbose_name='Added Date', auto_now_add=True)),
                ('modified_time', models.DateTimeField(verbose_name='Modified Date', auto_now=True)),
            ],
            options={
                'db_table': 'reports',
            },
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(verbose_name='Name', max_length=50)),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('url', models.URLField(verbose_name='Service URL', default='', max_length=50)),
                ('created_time', models.DateTimeField(verbose_name='Added Date', auto_now_add=True)),
                ('modified_time', models.DateTimeField(verbose_name='Modified Date', auto_now=True)),
            ],
            options={
                'db_table': 'services',
            },
        ),
        migrations.AddField(
            model_name='report',
            name='service',
            field=models.ForeignKey(related_name='report_service', to='service.Service'),
        ),
        migrations.AddField(
            model_name='report',
            name='users',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
    ]
