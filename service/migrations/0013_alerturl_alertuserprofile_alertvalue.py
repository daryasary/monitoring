# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('service', '0012_auto_20170305_1210'),
    ]

    operations = [
        migrations.CreateModel(
            name='AlertURL',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('url', models.URLField(verbose_name='Alert URL', max_length=255)),
                ('duration', models.DurationField(verbose_name='Duration')),
                ('is_enable', models.BooleanField(verbose_name='Is Enable', default=True)),
                ('created_time', models.DateTimeField(verbose_name='Created Time', auto_now_add=True)),
                ('modified_time', models.DateTimeField(auto_now=True, verbose_name='Modified Time')),
            ],
            options={
                'db_table': 'alert_url',
            },
        ),
        migrations.CreateModel(
            name='AlertUserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('message_sending_method', models.CharField(verbose_name='Message Sending Method', max_length=50, choices=[('email', 'EMAIL'), ('sms', 'SMS')])),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'alert_user_profile',
            },
        ),
        migrations.CreateModel(
            name='AlertValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('key', models.CharField(verbose_name='Key', max_length=50)),
                ('value_operator', models.CharField(verbose_name='Value Operator', max_length=10, choices=[('>', '>'), ('>=', '>='), ('<', '<'), ('<=', '<='), ('==', '=='), ('!=', '!=')])),
                ('value', models.CharField(verbose_name='Value', max_length=10, help_text='Specify Alert Value as an Integer and select an operator')),
                ('alert_url', models.ForeignKey(to='service.AlertURL')),
                ('users', models.ManyToManyField(help_text='Users that will be informed by this alert.', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'alert_value',
            },
        ),
    ]
