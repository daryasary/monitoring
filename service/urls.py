from django.conf.urls import url

from .views import UserLoginView, MenuView, ChangePassword, AddRemoveReportView, \
    ReportView, FilterView, ActionView, FormApplyView


urlpatterns = [
    # Authentication
    url(r'^login/$', UserLoginView.as_view(), name='login'),  # login and logout
    url(r'^change_password/$', ChangePassword.as_view(), name='change-password'),

    # CRUD Reports
    url(r'^add_remove_reports/(?P<report_id>[0-9]*)$', AddRemoveReportView.as_view(), name='add-remove-reports'),

    # Reports
    url(r'^menu/$', MenuView.as_view(), name='menu'),
    url(r'^reports/(?P<report_id>[0-9]+)$', ReportView.as_view(), name='reports'),
    url(r'^reports/(?P<report_id>[0-9]+)/filter/$', FilterView.as_view(), name='reports-filters'),
    url(r'^reports/(?P<report_id>[0-9]+)/action/$', ActionView.as_view(), name='reports-actions'),

    # Forms
    url(r'^forms/(?P<report_id>[0-9]+)$', FormApplyView.as_view(), name='forms-view'),
]
