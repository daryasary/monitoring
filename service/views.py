import logging
import requests
from collections import OrderedDict

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.utils.translation import ugettext as _
from django.utils.decorators import method_decorator
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import FormView

from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework import status

from .models import Report, Service
from .forms import AddRemoveUserOfReportsForm
from .serializers import ServiceSerializer, ReportSerializer
from utils.reports_data_converters import data_converter
from utils.authentication import check_login


logger_login_logout = logging.getLogger('login_logout')
logger_requests = logging.getLogger('reports_requests')
logger_add_remove = logging.getLogger('reports_add_remove')


def log_generator(logger, api_name, user, report_id=None, msg=None, extra_data=None):
    logger('[%s]-[%s]-[%s]-[%s]-[%s]' % (api_name, user, report_id, msg, extra_data))


def get_report(request, report_id, api_name):
    response = {
        'msg': '',
        'report': None,
        'status': status.HTTP_200_OK
    }

    try:
        response['report'] = Report.live.get(pk=report_id)
    except Report.DoesNotExist:
        msg = _('Report not found')
        response['msg'] = msg
        response['status'] = status.HTTP_404_NOT_FOUND
        log_generator(logger_requests.error, api_name, request.user, report_id, msg, dict(request.data))
    else:
        if not response['report'].user_access(request.user):
            msg = _('User has not access to this report')
            response['msg'] = msg
            response['report'] = None
            response['status'] = status.HTTP_401_UNAUTHORIZED
            log_generator(logger_requests.warning, api_name, request.user, report_id, msg, dict(request.data))

    return response


class CsrfExemptSessionAuthentication(SessionAuthentication):
    """
    CSRF Exempt for Session Base Authentication.
    """
    def enforce_csrf(self, request):
        return


class UserLoginView(APIView):
    """
    Users login and logout.
    Used POST for login and DELETE for logout.
    """
    authentication_classes = (CsrfExemptSessionAuthentication,)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request):
        response = {
            'success': False,
            'last_login': None,
            'first_name': None,
            'last_name': None
        }

        username = request.data.get('username')
        password = request.data.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            response['success'] = True
            response['last_login'] = user.last_login
            response['first_name'] = user.first_name
            response['last_name'] = user.last_name
        log_generator(logger_login_logout.info, 'login', user, extra_data=response)
        return Response(response, status=status.HTTP_200_OK if user else status.HTTP_403_FORBIDDEN)

    @method_decorator(check_login)
    def delete(self, request):
        msg = _('User is logout.')
        log_generator(logger_login_logout.info, 'logout', request.user, msg=msg)
        logout(request)
        return Response({'detail': msg}, status=status.HTTP_200_OK)


class ChangePassword(APIView):
    """
    Change password when user is in panel. (No logout required)
    """
    authentication_classes = (CsrfExemptSessionAuthentication,)

    @method_decorator(csrf_exempt)
    @method_decorator(check_login)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request):
        old_password = request.data.get('old_password', '')
        new_password1 = request.data.get('new_password1', '')
        new_password2 = request.data.get('new_password2', '')

        if not request.user.check_password(old_password):
            return Response({'detail': _('Wrong old password.')}, status=status.HTTP_400_BAD_REQUEST)
        if not new_password1 or new_password1 != new_password2:
            return Response({'detail': _('New passwords are not match.')}, status=status.HTTP_400_BAD_REQUEST)

        request.user.set_password(new_password1)
        request.user.save()
        update_session_auth_hash(request, request.user)

        return Response({'detail': _('Password changed successfully.')}, status=status.HTTP_200_OK)


class MenuView(APIView):
    """
    Show menu of services and their reports that user has access to them.
    Superusers has access to all services and their reports.
    """
    authentication_classes = (CsrfExemptSessionAuthentication,)

    @method_decorator(csrf_exempt)
    @method_decorator(check_login)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        reports = Report.live.all()

        if not request.user.is_superuser:
            reports = Report.live.filter(users__username=request.user)

        available_reports = reports.values(
            'id',
            'title',
            'slug',
            'description',
            'service__title'
        ).order_by('title')

        menu = {}
        for report in available_reports:
            if not menu.get(report['service__title']):
                menu[report['service__title']] = []
            menu[report['service__title']].append(
                {
                    'id': report['id'],
                    'title': report['title'],
                    'slug': report['slug'],
                    'description': report['description'],
                    'service_title': report['service__title']
                }
            )

        return Response(OrderedDict(sorted(menu.items())))


class AddRemoveReportView(APIView):
    """
    API for reporters to add, remove, update and delete their reports.
    """
    authentication_classes = (CsrfExemptSessionAuthentication,)

    @method_decorator(check_login)
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, report_id):
        if report_id:
            try:
                report = Report.objects.get(id=report_id, service__owner=request.user)
            except Report.DoesNotExist:
                msg = _('User is not owner of this report')
                log_generator(logger_add_remove.error, 'get', request.user, msg=msg)
                return Response({'detail': msg}, status=status.HTTP_401_UNAUTHORIZED)
            else:
                serializer = ReportSerializer(report)

        else:
            services_reports = Service.objects.filter(owner=request.user)
            serializer = ServiceSerializer(services_reports, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, report_id):
        title = request.data.get('title', None)
        slug = request.data.get('slug', '')
        url = request.data.get('url', '')
        token = request.data.get('token', '')
        service_id = request.data.get('service_id', None)
        description = request.data.get('description', '')
        is_enable = request.data.get('is_enable', True)
        has_save = request.data.get('has_save', True)

        try:
            val = URLValidator()
            val(url)

            service = Service.objects.get(id=int(service_id), owner=request.user)

            report = Report.objects.create(
                title=title,
                slug=slug,
                url=url,
                token=token,
                service=service,
                description=description,
                is_enable=is_enable,
                has_save=has_save,
            )
        except ValidationError as e:
            msg = _(str(e.messages[0]))
            log_generator(logger_add_remove.error, 'post', request.user, report_id, msg, dict(request.data))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        except Service.DoesNotExist:
            msg = _('User is not owner of this service')
            log_generator(logger_add_remove.error, 'post', request.user, report_id, msg, dict(request.data))
            return Response({'detail': msg}, status=status.HTTP_401_UNAUTHORIZED)

        except Exception as e:
            msg = _('Could not create report. Error: %s' % str(e))
            log_generator(logger_add_remove.error, 'post', request.user, report_id, msg, dict(request.data))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        report.users.add(request.user)
        serializer = ReportSerializer(report)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def put(self, request, report_id):
        try:
            report = Report.objects.get(
                id=report_id if report_id else None,
                service__owner=request.user
            )
        except Report.DoesNotExist:
            msg = _('User is not owner of this report')
            log_generator(logger_add_remove.error, 'put', request.user, report_id, msg, dict(request.data))
            return Response({'detail': msg}, status=status.HTTP_401_UNAUTHORIZED)

        serializer = ReportSerializer(report, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        log_generator(logger_add_remove.error, 'put', request.user, report_id, 'serializer error', dict(request.data))

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, report_id):
        try:
            report = Report.objects.get(
                id=report_id if report_id else None,
                service__owner=request.user
            )
        except Report.DoesNotExist:
            msg = _('User is not owner of this report')
            log_generator(logger_add_remove.error, 'delete', request.user, report_id, msg, dict(request.data))
            return Response({'detail': msg}, status=status.HTTP_401_UNAUTHORIZED)
        else:
            msg = _('Report deleted')
            report.delete()
            log_generator(logger_add_remove.info, 'delete', request.user, report_id, msg, dict(request.data))
            return Response({'detail': msg}, status=status.HTTP_200_OK)


class ReportView(APIView):
    """
    Return a report data to user if user has access to that report.
    """
    authentication_classes = (CsrfExemptSessionAuthentication,)

    @method_decorator(csrf_exempt)
    @method_decorator(check_login)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, report_id):
        api_name = 'get-report'

        report = get_report(request, report_id, api_name)
        if not report.get('report'):
            return Response({'detail': report['msg']}, status=report['status'])
        report = report['report']

        try:
            headers = {'Authorization': 'Token %s' % report.token}
            r = requests.get(report.url, headers=headers, timeout=60)
            r.raise_for_status()
            r_json = r.json()

        except requests.exceptions.HTTPError as e:
            msg = _('No content')
            log_generator(logger_requests.error, api_name, request.user, report_id, msg, str(e))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            msg = _('No content')
            log_generator(logger_requests.error, api_name, request.user, report_id, msg, str(e))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        try:
            r_converted = data_converter(r_json)
        except Exception as e:
            msg = _('No suitable contents to convert')
            log_generator(logger_requests.error, api_name, request.user, report_id, msg, str(e))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        return Response(r_converted)


class FilterView(APIView):
    """
     Apply a filter on report that user has access to it.
     """
    authentication_classes = (CsrfExemptSessionAuthentication,)

    @method_decorator(csrf_exempt)
    @method_decorator(check_login)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, report_id):
        api_name = 'post-filter'

        report = get_report(request, report_id, api_name)
        if not report.get('report'):
            return Response({'detail': report['msg']}, status=report['status'])
        report = report['report']

        filter_values = request.data.get('filter_values', {})
        filter_values['filter_slug'] = request.data.get('filter_slug', '')

        try:
            headers = {'Authorization': 'Token %s' % report.token}
            r = requests.post(report.url, headers=headers, json=filter_values, timeout=60)
            r.raise_for_status()
            r_json = r.json()

        except requests.exceptions.HTTPError as e:
            msg = _('No content')
            log_generator(logger_requests.error, api_name, request.user, report_id, msg, str(e))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            msg = _('No content')
            log_generator(logger_requests.error, api_name, request.user, report_id, msg, str(e))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        try:
            r_converted = data_converter(r_json)
        except Exception as e:
            msg = _('No suitable contents to convert')
            log_generator(logger_requests.error, api_name, request.user, report_id, msg, str(e))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        return Response(r_converted)


class ActionView(APIView):
    """
    Apply an action on a report row (mainly in table reports).
    """
    authentication_classes = (CsrfExemptSessionAuthentication,)

    @method_decorator(csrf_exempt)
    @method_decorator(check_login)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, report_id):
        api_name = 'post-action'
        actions = request.data.get('actions', None)
        log_generator(logger_requests.info, api_name, request.user, report_id, 'action request', actions)

        if not actions:
            msg = _('No Action Selected')
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        for k, v in actions.items():
            if not v:
                msg = _('Action Failed')
                log_generator(logger_requests.error, api_name, request.user, report_id, msg)
                return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        report = get_report(request, report_id, api_name)
        if not report.get('report'):
            return Response({'detail': report['msg']}, status=report['status'])
        report = report['report']

        try:
            headers = {'Authorization': 'Token %s' % report.token}
            r = requests.post(report.url, headers=headers, json={'actions': actions}, timeout=60)
            r.raise_for_status()
            r_json = r.json()
        except Exception as e:
            msg = _('Action Failed')
            log_generator(logger_requests.error, api_name, request.user, report_id, msg, str(e))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)
        else:
            detail = r_json.get('detail')
            log_generator(logger_requests.info, api_name, request.user, report_id, detail, actions)
            return Response({'detail': detail}, status=r.status_code)

        # if r.status_code // 100 == 2:
        #     detail = r_json.get('detail') or _('Action Applied')
        # else:
        #     detail = r_json.get('detail') or _('Action Failed')


class FormApplyView(APIView):
    """
    Apply a form request from client (check that user has access).
    """
    authentication_classes = (CsrfExemptSessionAuthentication,)

    @method_decorator(csrf_exempt)
    @method_decorator(check_login)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, report_id):
        api_name = 'post-form'

        report = get_report(request, report_id, api_name)
        if not report.get('report'):
            return Response({'detail': report['msg']}, status=report['status'])
        report = report['report']

        request_data = dict(request.data)
        request_data['user_id'] = request.user.id
        files = {k: v.read() for k, v in request.FILES.items()}

        try:
            headers = {'Authorization': 'Token %s' % report.token}
            r = requests.post(report.url, headers=headers, data=request_data, files=files, timeout=60)
            r.raise_for_status()
            r_json = r.json()

        except requests.exceptions.HTTPError as e:
            msg = _('No content')
            log_generator(logger_requests.error, api_name, request.user, report_id, msg, str(e))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            msg = _('No content')
            log_generator(logger_requests.error, api_name, request.user, report_id, msg, str(e))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response(r_json, status=r.status_code)


class AddRemoveUserOfReportsView(FormView):
    template_name = 'add_remove_user_of_reports.html'
    form_class = AddRemoveUserOfReportsForm
    model = Report

    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'request': self.request})

        if self.kwargs.get('add_or_remove') == 'add':
            kwargs.update({'form_name': 'Add User to Reports'})
        elif self.kwargs.get('add_or_remove') == 'remove':
            kwargs.update({'form_name': 'Remove User from Reports'})

        reports_ids = self.request.get_full_path().split('=')[-1].split(',')
        reports_ids = [int(r) for r in reports_ids]

        self.kwargs.update({'reports_ids': reports_ids})

        return kwargs

    def form_valid(self, form):
        self.success_url = '/api/admin/%s/%s/' % (self.model._meta.app_label, self.model._meta.model_name)

        user = int(self.request.POST.get('user'))
        reports_ids = self.kwargs.get('reports_ids')

        if self.kwargs.get('add_or_remove') == 'add':
            for report_id in reports_ids:
                report = Report.objects.get(id=report_id)
                report.users.add(user)
            messages.add_message(self.request, messages.SUCCESS, 'User added to reports successfully.')

        elif self.kwargs.get('add_or_remove') == 'remove':
            for report_id in reports_ids:
                report = Report.objects.get(id=report_id)
                report.users.remove(user)
            messages.add_message(self.request, messages.SUCCESS, 'User removed from reports successfully.')

        return super().form_valid(form)
