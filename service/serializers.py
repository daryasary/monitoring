from rest_framework import serializers

from .models import Service, Report


class ReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Report
        fields = ('id', 'title', 'description', 'slug', 'service', 'url', 'is_enable', 'has_save', 'token')

        extra_kwargs = {
            'id': {'read_only': True},
            'service': {'read_only': True},
        }


class ServiceSerializer(serializers.ModelSerializer):
    reports = serializers.SerializerMethodField()

    class Meta:
        model = Service
        fields = ('id', 'title', 'description', 'owner', 'reports')

        extra_kwargs = {
            'id': {'read_only': True},
            'owner': {'read_only': True},
        }

    def get_reports(self, instance):
        rep = Report.objects.filter(service=instance)
        return ReportSerializer(rep, many=True).data



