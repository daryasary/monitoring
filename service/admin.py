from django.contrib import admin
from django.http import HttpResponseRedirect

from .models import Service, Report, AlertUserProfile, Alert, AlertValue, AlertSendMessageLog, SMSSendLog
from .views import AddRemoveUserOfReportsView


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'owner', 'url', 'created_time', 'modified_time')


@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'service', 'url', 'is_enable', 'has_save', 'users_that_have_access')
    list_filter = ('is_enable', 'has_save', 'service')
    search_fields = ('title', 'slug', 'users__username')
    filter_horizontal = ('users', )
    actions = ('add_users_to_reports', 'remove_users_from_reports')

    def users_that_have_access(self, obj):
        return ',\n'.join([u.username for u in obj.users.all()])

    def get_urls(self):
        from django.conf.urls import patterns, url

        urls = super().get_urls()
        my_urls = patterns(
            '',
            url(r'^add_users_to_reports/$', AddRemoveUserOfReportsView.as_view(), {'add_or_remove': 'add'}),
            url(r'^remove_users_from_reports/$', AddRemoveUserOfReportsView.as_view(), {'add_or_remove': 'remove'}),
        )
        return my_urls + urls

    def add_users_to_reports(self, request, queryset):
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return HttpResponseRedirect('%sadd_users_to_reports/?ids=%s' % (request.META.get('PATH_INFO'), ','.join(selected)))
    add_users_to_reports.short_description = 'Add User to Reports'

    def remove_users_from_reports(self, request, queryset):
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return HttpResponseRedirect('%sremove_users_from_reports/?ids=%s' % (request.META.get('PATH_INFO'), ','.join(selected)))
    remove_users_from_reports.short_description = 'Remove User from Reports'


@admin.register(AlertUserProfile)
class AlertUserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'message_sending_method')
    list_filter = ('message_sending_method', )
    search_fields = ('user__username', )


class AlertValueInline(admin.TabularInline):
    model = AlertValue
    extra = 0


@admin.register(Alert)
class AlertAdmin(admin.ModelAdmin):
    list_display = ('title', 'url', 'duration', 'is_enable')
    list_filter = ('is_enable', )
    search_fields = ('title', )
    inlines = (AlertValueInline, )


@admin.register(AlertSendMessageLog)
class AlertSendMessageLogAdmin(admin.ModelAdmin):
    list_display = ('alert_value', 'user', 'send_method', 'send_status', 'send_time', 'description')
    list_filter = ('send_method', 'send_status')
    date_hierarchy = 'send_time'
    actions = None
    readonly_fields = ('alert_value', 'user', 'send_method', 'send_status', 'send_time', 'description')

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False


@admin.register(SMSSendLog)
class SMSSendLogAdmin(admin.ModelAdmin):
    list_display = ('user', 'phone_number', 'message', 'send_status', 'send_time', 'description')
    list_filter = ('send_status', )
    search_fields = ('user__username', 'phone_number')
    date_hierarchy = 'send_time'
    actions = None
    readonly_fields = ('user', 'phone_number', 'message', 'send_status', 'send_time', 'description')

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False
