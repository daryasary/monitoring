import requests
from mongoengine import *
from datetime import datetime, timedelta

from django.conf import settings
from django.core.management.base import BaseCommand

from service.models import Report
# from service.mongo_db import ReportData


connect(
    settings.MONGO_DB,
    host=settings.MONGO_HOST,
    username=settings.MONGO_USER,
    password=settings.MONGO_PASS,
    connect=False
)


class ReportData(Document):
    report_id = IntField()
    report_data = DictField()
    date = DateTimeField(default=datetime.now().date()-timedelta(1))


class Command(BaseCommand):
    help = "Save reports to MongoDB daily using crontab."

    def handle(self, *args, **options):
        for report in Report.objects.filter(has_save=True):
            try:
                headers = {'Authorization': 'Token %s' % report.service.token}
                response = requests.get(report.url, headers=headers)
                result = response.json()
            except requests.exceptions.RequestException as e:
                self.stdout.write("Report %s could not be fetched. %s. %s" % (report.id, str(datetime.now()), str(e)))
            else:
                ReportData(
                    report_id=report.id,
                    report_data=result
                ).save()
                self.stdout.write("Report %s saved to MongoDB. %s" % (report.id, str(datetime.now())))
