import requests
import logging

from pymongo import MongoClient

from django.core.management.base import BaseCommand
from django.conf import settings
from django.utils import timezone

from service.models import Report


logger = logging.getLogger('save_reports_mongo')


class Command(BaseCommand):
    help = "Save reports to MongoDB daily using crontab. - (by pymongo)"

    def add_arguments(self, parser):
        parser.add_argument('--reports', nargs='*', type=int, default=0, help='Ids of reports')

    def handle(self, *args, **options):
        client = MongoClient(settings.MONGO_HOST)
        client[settings.MONGO_DB].authenticate(settings.MONGO_USER, settings.MONGO_PASS, mechanism='MONGODB-CR')
        db = client[settings.MONGO_DB]

        if options['reports'] == 0:
            reports = Report.objects.filter(has_save=True)
        else:
            reports = Report.objects.filter(has_save=True, id__in=options['reports'])

        logger.info('reports: %s' % list(reports.values_list('id', flat=True)))

        for report in reports:
            try:
                headers = {'Authorization': 'Token %s' % report.service.token}
                response = requests.get(report.url, headers=headers)
                result = response.json()
            except Exception as e:
                logger.info('[%s]-[report_id=%s] Saving Error: %s' % (str(timezone.now()), report.id, str(e)))
            else:
                collection = db['report_%s' % report.id]
                collection.insert_one(
                    {
                        'report_id': report.id,
                        'title': report.title,
                        'data': result,
                        'date': str(timezone.now().date() - timezone.timedelta(1))
                    }
                )
                logger.info('[%s]-[report_id=%s] Saving successful.' % (str(timezone.now()), report.id))
