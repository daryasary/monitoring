from datetime import datetime

from django.core.management.base import BaseCommand

from service.models import Alert
from service.tasks import start_alert


class Command(BaseCommand):
    help = "Run Alert Tasks Manually (after reset celery queue)."

    def handle(self, *args, **options):
        for alert in Alert.objects.get(is_enable=True):
            next_run = datetime.utcnow() + alert.duration
            start_alert.apply_async((alert.id,), eta=next_run)
            self.stdout.write('Alert task for %s, %s with duration %s is running.' % (alert.id, alert.title, alert.duration))
