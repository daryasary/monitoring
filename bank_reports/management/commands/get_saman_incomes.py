import json
import logging

from django.core.management.base import BaseCommand
from django.utils import timezone

from bank_reports.models import Gateway, Terminal, Income
from bank_reports.gateways.saman import saman_incomes


logger = logging.getLogger('saman_bank')


class Command(BaseCommand):
    help = 'Get incomes of a day.'

    def add_arguments(self, parser):
        parser.add_argument('--days', nargs='?', type=int, default=1, help='Number of days to get their incomes')

    def handle(self, *args, **options):
        from_date = timezone.now().date() - timezone.timedelta(days=options['days']+1)
        to_date = timezone.now().date() - timezone.timedelta(days=1)
        terminals = Terminal.objects.select_related('gateway').filter(is_enable=True, gateway__gw_code=Gateway.SAMAN)

        for terminal in terminals:
            credentials = json.loads(terminal.credentials)
            result = saman_incomes(
                terminal.gateway.url,
                credentials.get('username'),
                credentials.get('password'),
                credentials.get('terminal_id'),
                from_date,
                to_date
            )

            for res in result:
                Income.objects.create(
                    terminal=terminal,
                    tr_date=res['tr_date'],
                    tr_date_shamsi=res['tr_date_shamsi'],
                    amount=res['amount'],
                    term_id=res['term_id'],
                    result_raw=res['result_raw'],
                    result_parsed=json.dumps(res['result_parsed'])
                )

            logger.info('[%s]-[term_id=%s]-[incomes]-%s incomes added' % (str(timezone.now()), terminal.id, len(result)))
