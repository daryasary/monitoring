import json
import logging

from django.core.management.base import BaseCommand
from django.utils import timezone

from bank_reports.models import Gateway, Service, Terminal, Transaction
from bank_reports.gateways.saman import saman_transactions


logger = logging.getLogger('saman_bank')


class Command(BaseCommand):
    help = 'Get transaction of a day.'

    def add_arguments(self, parser):
        parser.add_argument('--days', nargs='?', type=int, default=1, help='Number of days to get their transactions')

    def handle(self, *args, **options):
        for i in range(options['days'], 0, -1):
            tr_date = timezone.now().date() - timezone.timedelta(days=i)
            terminals = Terminal.objects.select_related('gateway').filter(is_enable=True, gateway__gw_code=Gateway.SAMAN)
            srv = Service.objects.filter(is_enable=True, terminal__gateway__gw_code=Gateway.SAMAN).values('id', 'slug')
            services = {s['slug']: s['id'] for s in srv}

            for terminal in terminals:
                credentials = json.loads(terminal.credentials)
                result = saman_transactions(
                    terminal.gateway.url,
                    credentials.get('username'),
                    credentials.get('password'),
                    credentials.get('terminal_id'),
                    tr_date
                )

                for res in result:
                    service = services.get(res.get('slug'), None)
                    Transaction.objects.create(
                        service_id=service,
                        terminal=terminal,
                        tr_date=tr_date,
                        amount=res['amount'],
                        term_id=res['term_id'],
                        slug=res['slug'],
                        result_raw=res['result_raw'],
                        result_parsed=json.dumps(res['result_parsed'])
                    )

                logger.info('[%s]-[term_id=%s]-[transactions]-[tr_date=%s]-%s transactions added' % (
                    str(timezone.now()), terminal.id, tr_date, len(result)))

                # for service in services:
                #     for res in result:
                #         if not res.get('slug'):
                #             print(res)
                        # if res.get('slug') == service.slug:
                        #     Transaction.objects.create(
                        #         service=service,
                        #         terminal=terminal,
                        #         tr_date=tr_date,
                        #         amount=res['amount'],
                        #         term_id=res['term_id'],
                        #         slug=res['slug'],
                        #         result_raw=res['result_raw'],
                        #         result_parsed=json.dumps(res['result_parsed'])
                        #     )
