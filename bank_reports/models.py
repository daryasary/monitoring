from django.db import models
from django.utils.translation import gettext_lazy as _


class Gateway(models.Model):
    SAMAN = 1
    GATEWAY_CHOICES = (
        (SAMAN, _('Saman')),
    )

    created_time = models.DateTimeField(_('creation on'), auto_now_add=True)
    updated_time = models.DateTimeField(_('modified on'), auto_now=True)
    title = models.CharField(_('title'), max_length=50)
    gw_code = models.PositiveSmallIntegerField(_('gateway code'), choices=GATEWAY_CHOICES)
    url = models.CharField(_('url'), max_length=250, blank=True)

    class Meta:
        db_table = 'bank_gateways'

    def __str__(self):
        return self.title


class Terminal(models.Model):
    created_time = models.DateTimeField(_('creation on'), auto_now_add=True)
    updated_time = models.DateTimeField(_('modified on'), auto_now=True)
    gateway = models.ForeignKey(Gateway, verbose_name=_('gateway'), related_name='terminals')
    title = models.CharField(_('title'), max_length=100)
    credentials = models.TextField(_('credentials'), blank=True)
    is_enable = models.BooleanField(_('is enable'), default=True)

    class Meta:
        db_table = 'bank_terminals'

    def __str__(self):
        return '%s - %s' % (self.gateway.title, self.title)


class Service(models.Model):
    created_time = models.DateTimeField(_('creation on'), auto_now_add=True)
    updated_time = models.DateTimeField(_('modified on'), auto_now=True)
    terminal = models.ManyToManyField(Terminal, verbose_name=_('terminal'), related_name='services')
    title = models.CharField(_('title'), max_length=100)
    slug = models.CharField(_('slug'), max_length=50)
    is_enable = models.BooleanField(_('is enable'), default=True)

    class Meta:
        db_table = 'bank_services'

    def __str__(self):
        return self.title


class Transaction(models.Model):
    created_time = models.DateTimeField(_('creation on'), auto_now_add=True)
    service = models.ForeignKey(Service, verbose_name=_('service'), related_name='transactions_services', blank=True, null=True)
    terminal = models.ForeignKey(Terminal, verbose_name=_('terminal'), related_name='transactions_terminals')
    tr_date = models.DateField(_('transaction date'), db_index=True)
    amount = models.PositiveIntegerField(_('amount'))
    term_id = models.CharField(_('terminal id'), max_length=50, blank=True, db_index=True)
    slug = models.CharField(_('slug'), max_length=50, blank=True, db_index=True)
    result_raw = models.TextField(_('transaction result raw'), blank=True)
    result_parsed = models.TextField(_('transaction result parsed'), blank=True)

    class Meta:
        db_table = 'bank_transactions'


class Income(models.Model):
    created_time = models.DateTimeField(_('creation on'), auto_now_add=True)
    terminal = models.ForeignKey(Terminal, verbose_name=_('terminal'), related_name='incomes_terminals')
    tr_date = models.DateField(_('transaction date'), db_index=True)
    tr_date_shamsi = models.CharField(_('transaction date shamsi'), max_length=20, blank=True)
    amount = models.PositiveIntegerField(_('amount'))
    term_id = models.CharField(_('terminal id'), max_length=50, blank=True, db_index=True)
    result_raw = models.TextField(_('income result raw'), blank=True)
    result_parsed = models.TextField(_('income result parsed'), blank=True)

    class Meta:
        db_table = 'bank_incomes'
