# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Gateway',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('created_time', models.DateTimeField(verbose_name='creation on', auto_now_add=True)),
                ('updated_time', models.DateTimeField(verbose_name='modified on', auto_now=True)),
                ('title', models.CharField(verbose_name='title', max_length=50)),
                ('gw_code', models.PositiveSmallIntegerField(verbose_name='gateway code', choices=[(1, 'Saman')])),
                ('url', models.CharField(verbose_name='url', blank=True, max_length=250)),
            ],
            options={
                'db_table': 'bank_gateways',
            },
        ),
        migrations.CreateModel(
            name='Income',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('created_time', models.DateTimeField(verbose_name='creation on', auto_now_add=True)),
                ('tr_date', models.DateField(verbose_name='transaction date', db_index=True)),
                ('tr_date_shamsi', models.CharField(verbose_name='transaction date shamsi', blank=True, max_length=20)),
                ('amount', models.PositiveIntegerField(verbose_name='amount')),
                ('term_id', models.CharField(verbose_name='terminal id', db_index=True, blank=True, max_length=50)),
                ('result_raw', models.TextField(verbose_name='income result raw', blank=True)),
                ('result_parsed', models.TextField(verbose_name='income result parsed', blank=True)),
            ],
            options={
                'db_table': 'bank_incomes',
            },
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('created_time', models.DateTimeField(verbose_name='creation on', auto_now_add=True)),
                ('updated_time', models.DateTimeField(verbose_name='modified on', auto_now=True)),
                ('title', models.CharField(verbose_name='title', max_length=100)),
                ('slug', models.CharField(verbose_name='slug', max_length=50)),
                ('is_enable', models.BooleanField(verbose_name='is enable', default=True)),
            ],
            options={
                'db_table': 'bank_services',
            },
        ),
        migrations.CreateModel(
            name='Terminal',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('created_time', models.DateTimeField(verbose_name='creation on', auto_now_add=True)),
                ('updated_time', models.DateTimeField(verbose_name='modified on', auto_now=True)),
                ('title', models.CharField(verbose_name='title', max_length=100)),
                ('credentials', models.TextField(verbose_name='credentials', blank=True)),
                ('is_enable', models.BooleanField(verbose_name='is enable', default=True)),
                ('gateway', models.ForeignKey(verbose_name='gateway', to='bank_reports.Gateway', related_name='terminals')),
            ],
            options={
                'db_table': 'bank_terminals',
            },
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('created_time', models.DateTimeField(verbose_name='creation on', auto_now_add=True)),
                ('tr_date', models.DateField(verbose_name='transaction date', db_index=True)),
                ('amount', models.PositiveIntegerField(verbose_name='amount')),
                ('term_id', models.CharField(verbose_name='terminal id', db_index=True, blank=True, max_length=50)),
                ('slug', models.CharField(verbose_name='slug', db_index=True, blank=True, max_length=50)),
                ('result_raw', models.TextField(verbose_name='transaction result raw', blank=True)),
                ('result_parsed', models.TextField(verbose_name='transaction result parsed', blank=True)),
                ('service', models.ForeignKey(verbose_name='service', to='bank_reports.Service', related_name='transactions_services')),
                ('terminal', models.ForeignKey(verbose_name='terminal', to='bank_reports.Terminal', related_name='transactions_terminals')),
            ],
            options={
                'db_table': 'bank_transactions',
            },
        ),
        migrations.AddField(
            model_name='service',
            name='terminal',
            field=models.ManyToManyField(verbose_name='terminal', related_name='services', to='bank_reports.Terminal'),
        ),
        migrations.AddField(
            model_name='income',
            name='terminal',
            field=models.ForeignKey(verbose_name='terminal', to='bank_reports.Terminal', related_name='incomes_terminals'),
        ),
    ]
