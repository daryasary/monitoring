# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bank_reports', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='service',
            field=models.ForeignKey(related_name='transactions_services', blank=True, null=True, verbose_name='service', to='bank_reports.Service'),
        ),
    ]
