from django.contrib import admin

from .models import Gateway, Terminal, Service, Transaction, Income


@admin.register(Gateway)
class GatewayAdmin(admin.ModelAdmin):
    list_display = ('title', 'gw_code', 'url')


@admin.register(Terminal)
class TerminalAdmin(admin.ModelAdmin):
    list_display = ('title', 'gateway', 'is_enable', 'credentials')


@admin.register(Service)
class Service(admin.ModelAdmin):
    list_display = ('title', 'slug', 'is_enable', 'terminals')
    list_filter = ('terminal', )
    filter_horizontal = ('terminal', )

    def terminals(self, obj):
        return ',\n'.join([t.title for t in obj.terminal.all()])


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ('tr_date', 'amount', 'service', 'terminal', 'term_id', 'slug')
    list_filter = ('service', 'terminal', 'slug')
    date_hierarchy = 'tr_date'
    actions = None

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]


@admin.register(Income)
class IncomeAdmin(admin.ModelAdmin):
    list_display = ('tr_date', 'amount', 'terminal', 'term_id', )
    list_filter = ('terminal', )
    ordering = ('-tr_date', )
    date_hierarchy = 'tr_date'
    actions = None

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]
