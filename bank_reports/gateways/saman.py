import logging
from zeep import Client
from zeep.transports import Transport
from khayyam import JalaliDate

from django.utils import timezone


logger = logging.getLogger(__name__)


def saman_transactions_xml_parser(wsdl_result):
    result = []
    result_params = [
        'LeagalPan', 'TerminalId', 'TransactionDate', 'TransactionTime', 'Amount', 'ReferenceNumber',
        'ResNum', 'Wage', 'ResNum1', 'ResNum2', 'ResNum3', 'ResNum4', 'RRN', 'TraceNo'
    ]

    for result_raw in wsdl_result.strip().split('\n\n'):
        result_parsed = {k: v for k, v in zip(result_params, result_raw.split('|'))}

        slug = 'non-service'
        for i in range(4, 0, -1):
            slug = result_parsed.get('ResNum%s' % i)
            if slug:
                break

        result.append(
            {
                'result_raw': result_raw,
                'result_parsed': result_parsed,
                'amount': int(result_parsed['Amount']),
                'term_id': result_parsed['TerminalId'],
                'slug': slug
            }
        )

    return result


def saman_incomes_xml_parser(wsdl_result):
    result = []
    result_params = [
        'deposit_date', 'shaba_number', 'paya_deposit_code', 'total_amount', 'terminal_id', 'deposit_time'
    ]

    for result_raw in wsdl_result.strip().replace(' ', '').split('\n'):
        result_parsed = {k: v for k, v in zip(result_params, result_raw.split('|'))}
        d = result_parsed['deposit_date']
        # todo: must be updated to support years after 1400
        dd = JalaliDate(int('13'+d[:2]), int(d[2:4]), int(d[4:])).todate()
        result.append(
            {
                'result_raw': result_raw,
                'result_parsed': result_parsed,
                'amount': int(result_parsed['total_amount']),
                'term_id': result_parsed['terminal_id'],
                'tr_date': dd,
                'tr_date_shamsi': d
            }
        )

    return result


def saman_transactions(wsdl_url, username, password, terminal_id, tr_date):
    wsdl_params = {
        'UserName': username,
        'Password': password,
        'TermId': terminal_id,
        'TrDate': tr_date
    }

    transport = Transport()
    transport.session.headers = {'User-Agent': ''}  # 'curl/7.52.1'

    try:
        client = Client(wsdl=wsdl_url, transport=transport)
        wsdl_result = client.service.GetPgSucceedTransaction(**wsdl_params)
        if '-1;' in wsdl_result:
            raise Exception('Authentication Faild')
        result = saman_transactions_xml_parser(wsdl_result)
    except Exception as e:
        logger.error('[%s]-[saman transactions]-[term_id=%s]-[tr_date=%s]-Err=%s' %
                     (str(timezone.now()), terminal_id, str(tr_date), str(e)))
        return []

    return result


def saman_incomes(wsdl_url, username, password, terminal_id, from_date, to_date):
    wsdl_params = {
        '_UserName': username,
        '_Password': password,
        'TerminalID': terminal_id,
        'fromShamsiDate': JalaliDate(from_date),
        'toShamsiDate': JalaliDate(to_date)
    }

    transport = Transport()
    transport.session.headers = {'User-Agent': ''}  # 'curl/7.52.1'

    try:
        client = Client(wsdl=wsdl_url, transport=transport)
        wsdl_result = client.service.GetShaparakDailyReportIncomingHeader(**wsdl_params)
        if '-1;' in wsdl_result:
            raise Exception('Authentication Faild')
        result = saman_incomes_xml_parser(wsdl_result)
    except Exception as e:
        logger.error('[%s]-[saman incomes]-[term_id=%s]-[from_date=%s to_date=%s]-Err=%s' % (
            str(timezone.now()), terminal_id, str(from_date), str(to_date), str(e)))
        return []

    return result
