# from khayyam import JalaliDate
#
# from django.db.models import Sum
# from django.utils import timezone
#
# from rest_framework.response import Response
# from rest_framework.permissions import IsAuthenticated
# from rest_framework.authentication import TokenAuthentication
# from rest_framework.decorators import api_view, authentication_classes, permission_classes
#
# from utils.reports_patterns_generator import generate_report_pattern, chart_pattern, filter_pattern, text_pattern
# from .models import Gateway, Terminal, Service, Transaction, Income
#
#
# @api_view(['GET', 'POST'])
# @authentication_classes((TokenAuthentication, ))
# @permission_classes((IsAuthenticated, ))
# def saman_bank_daily_reports(request):
#     """
#     Reports for daily transactions and incomes of Saman Bank.
#     """
#     start_date = request.data.get('start_date', None)
#     end_date = request.data.get('end_date', None)
#     terminal = request.data.get('terminal', None)
#     service = request.data.get('service', None)
#
#     transaction_filter_params = dict()
#     transaction_filter_params['terminal__gateway__gw_code'] = Gateway.SAMAN
#     transaction_filter_params['tr_date__gte'] = start_date if start_date else timezone.now().date() - timezone.timedelta(days=6)
#     transaction_filter_params['tr_date__lt'] = end_date if end_date else timezone.now().date()
#     if service == 0:
#         transaction_filter_params['service__isnull'] = True
#     if service:
#         transaction_filter_params['service'] = service
#
#     if terminal:
#         transaction_filter_params['terminal'] = terminal
#
#     income_filter_params = dict()
#     income_filter_params['terminal__gateway__gw_code'] = Gateway.SAMAN
#     income_filter_params['tr_date__gte'] = start_date if start_date else timezone.now().date() - timezone.timedelta(days=6)
#     income_filter_params['tr_date__lt'] = end_date if end_date else timezone.now().date()
#     if terminal:
#         income_filter_params['terminal'] = terminal
#
#     transactions = Transaction.objects.filter(
#         **transaction_filter_params
#     ).values(
#         'tr_date', 'slug'
#     ).annotate(
#         Sum('amount')
#     )
#
#     incomes = Income.objects.filter(
#         **income_filter_params
#     ).values(
#         'tr_date', 'amount'
#     )
#
#     services = Service.objects.filter(
#         terminal__gateway__gw_code=Gateway.SAMAN,
#         is_enable=True
#     ).values(
#         'id', 'title'
#     )
#
#     terminals = Terminal.objects.filter(
#         gateway__gw_code=Gateway.SAMAN,
#         is_enable=True
#     ).values(
#         'id', 'title'
#     )
#
#     service_filter = [{'text': s['title'], 'value': s['id']} for s in services]
#     service_filter.append({'text': 'بدون سرویس', 'value': 0})
#     terminal_filter = [{'text': t['title'], 'value': t['id']} for t in terminals]
#
#     report = generate_report_pattern('گزارش مبالغ تراکنش ها و تسویه های بانک سامان', 'saman_bank_report')
#     report['filters'].append(filter_pattern('date', 'date_hijri', ['start_date', 'end_date']))  # date_hijri
#     report['filters'].append(filter_pattern('service', 'dropdown', service_filter))
#     report['filters'].append(filter_pattern('terminal', 'dropdown', terminal_filter))
#     transactions_daily_chart_linear = chart_pattern('تراکنش های روزانه', 'chart-linear', 'Days', 'Amounts')
#     transactions_daily_chart_stack = chart_pattern('تراکنش های روزانه', 'chart-stacked-bar', 'Days', 'Amounts')
#     incomes_daily_chart_bar = chart_pattern('تسویه های روزانه', 'chart-stacked-bar', 'Days', 'Amount')
#
#     for trans in transactions:
#         transactions_daily_chart_linear['graph_data']['data'].append(
#             {
#                 'category': str(JalaliDate(trans['tr_date'])),
#                 'name': trans['slug'],
#                 'value': trans['amount__sum']
#             }
#         )
#         transactions_daily_chart_stack['graph_data']['data'].append(
#             {
#                 'category': str(JalaliDate(trans['tr_date'])),
#                 'name': trans['slug'],
#                 'value': trans['amount__sum']
#             }
#         )
#
#     incomes_dict = {}
#     for ic in incomes:
#         incomes_dict[ic['tr_date']] = incomes_dict.get(ic['tr_date'], 0) + ic['amount']
#
#     for k in sorted(incomes_dict):
#         incomes_daily_chart_bar['graph_data']['data'].append(
#             {
#                 'category': str(JalaliDate(k)),
#                 'name': str(JalaliDate(k)),
#                 'value': incomes_dict[k]
#             }
#         )
#
#     report['data'].append(transactions_daily_chart_linear)
#     report['data'].append(transactions_daily_chart_stack)
#     report['data'].append(incomes_daily_chart_bar)
#
#     return Response(report)
#
#
# @api_view(['GET', 'POST'])
# @authentication_classes((TokenAuthentication, ))
# @permission_classes((IsAuthenticated, ))
# def saman_bank_total_reports(request):
#     """
#     Reports for total transactions and incomes of Saman Bank.
#     """
#     terminal = request.data.get('terminal', None)
#
#     filter_params = dict()
#     filter_params['terminal__gateway__gw_code'] = Gateway.SAMAN
#     if terminal:
#         filter_params['terminal'] = terminal
#
#     total_transactions = Transaction.objects.filter(
#         **filter_params
#     ).aggregate(Sum('amount'))['amount__sum']
#
#     total_incomes = Income.objects.filter(
#         **filter_params
#     ).aggregate(Sum('amount'))['amount__sum']
#
#     terminals = Terminal.objects.filter(
#         gateway__gw_code=Gateway.SAMAN,
#         is_enable=True
#     ).values(
#         'id', 'title'
#     )
#
#     terminal_filter = [{'text': t['title'], 'value': t['id']} for t in terminals]
#
#     report = generate_report_pattern('گزارش کل مبلغ تراکنش ها و تسویه های بانک سامان', 'saman_bank_report')
#     report['filters'].append(filter_pattern('terminal', 'dropdown', terminal_filter))
#     transactions_total_text = text_pattern('مجموع مبلغ تراکنش ها:')
#     incomes_total_text = text_pattern('مجموع مبلغ تسویه:')
#     remained_total_text = text_pattern('مجموع مبلغ بستانکاری:')
#
#     transactions_total_text['graph_data'] = total_transactions if total_transactions else 0
#     incomes_total_text['graph_data'] = total_incomes if total_incomes else 0
#     remained_total_text['graph_data'] = (total_transactions if total_transactions else 0) - (total_incomes if total_incomes else 0)
#
#     report['data'].append(transactions_total_text)
#     report['data'].append(incomes_total_text)
#     report['data'].append(remained_total_text)
#
#     return Response(report)
