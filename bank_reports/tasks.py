import json
from celery.schedules import crontab
from celery.task import periodic_task

from django.utils import timezone

from bank_reports.models import Gateway, Service, Transaction
from bank_reports.gateways.saman import saman_transactions


# todo: copy from commands

# @periodic_task(run_every=crontab(hour=5, minute=00))
def saman_bank_every_morning():
    pass
    # tr_date = timezone.now().date() - timezone.timedelta(days=1)
    # services = Service.objects.filter(terminal__gateway__gw_code=Gateway.SAMAN)
    #
    # for service in services:
    #     terminals = service.terminal.select_related('gateway').filter(gateway__gw_code=Gateway.SAMAN)
    #     for terminal in terminals:
    #         credentials = json.loads(terminal.credential)
    #         results = saman_transactions(
    #             terminal.gateway.url,
    #             credentials.get('username'),
    #             credentials.get('password'),
    #             credentials.get('terminal_id'),
    #             tr_date
    #         )
    #
    #         for res in results:
    #             if res.get('res_num1') == service.slug:
    #                 Transaction.objects.create(
    #                     service=service,
    #                     terminal=terminal,
    #                     tr_date=tr_date,
    #                     amount=res['amount'],
    #                     term_id=res['term_id'],
    #                     res_num1=res['res_num1'],
    #                     result_raw=res['result_raw'],
    #                     result_parsed=res['result_parsed']
    #                 )


# {"username": "C1714653", "password": "6155947", "terminal_id": 10807294}
#
# Saman   10795036     5350042     Fastcharge     zaferoon, fastcharge
# Saman   10807294     2073381     Picchap        photoapp
# Saman   10818968     9151802     Insta          instagram
# Saman   10848000     6004845     BestApp        bestapp


