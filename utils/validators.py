from django.conf import settings
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


class PhoneNumberValidator(RegexValidator):
    regex = '^98(9[0-3,9]\d{8}|[1-9]\d{9})$'
    message = _('Phone number must be a VALID 12 digits like 98xxxxxxxxxx')
    code = 'invalid_phone_number'


class SKUValidator(RegexValidator):
    regex = '^[a-zA-Z0-9\-]{6,20}$'
    message = _('SKU must be alphanumeric with 10 to 20 characters')
    code = 'invalid_sku'


class NumericValidator(RegexValidator):
    regex = '^[0-9]*$'
    message = _('Only numeric characters are allowed.')
    code = 'invalid_number'


class UsernameValidator(RegexValidator):
    regex = '^[a-zA-Z][a-zA-Z0-9_\.]+$'
    message = _('Enter a valid username starting with a-z. '
                'This value may contain only letters, numbers and underscore characters.'),
    code = 'invalid_username'


def image_size_validator(image_file):
    if image_file.file.size > settings.UPLOAD_IMAGE_MAX_SIZE:
        raise ValidationError(_('Max file size is %s MB' % (image_file.file.size / (1024 * 1024))))


sku_validator = SKUValidator()
phone_number_validator = PhoneNumberValidator()
numeric_validator = NumericValidator()
username_validator = UsernameValidator()
