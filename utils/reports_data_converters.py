
def data_converter(report):
    """
    A selector to choose best converter for each graph type.
    Converts graph data to suitable format for front-end view (High-charts).
    """
    report_data = report['data']
    converted_data = []

    for data in report_data:

        # tables --------------------------------------------------------------
        if data['graph_type'] == 'table':
            data['graph_data'] = table_converter(data['graph_data'])

        # any type of chart but custom ----------------------------------------
        elif data['graph_type'].endswith('custom'):
            data['graph_type'] = data['graph_type'].replace('-custom', '')

        # 3d charts -----------------------------------------------------------
        elif data['graph_type'] == 'chart-stacked-bar-3d':
            data['graph_data'] = chart_converter_3d(data['graph_data'])

        # charts --------------------------------------------------------------
        # chart-point
        # chart - linear
        # chart-column-bar
        # chart-stacked-bar
        # chart-stacked-bar-horizontal
        elif data['graph_type'].startswith('chart'):
            data['graph_data'] = chart_converter(data['graph_data'])

        # other types did not convert -----------------------------------------
        # else:
        #     data['graph_data'] = data['graph_data']

        converted_data.append(data)

    report['data'] = converted_data

    return report


def chart_converter(graph_data):
    """
    Converts chart data to suitable format for front-end view.
    """
    categories = {
        d['category'].replace("\\n", "<br>").replace("\t", "&nbsp")
        if isinstance(d, str) else d['category']
        for d in graph_data['data']
    }
    categories = sorted(list(categories))
    names = sorted(list({d['name'] for d in graph_data['data']}))
    result = {name: {
        'categories': categories,
        'data': [None for i in range(len(categories))]
        } for name in names
    }

    for data in graph_data['data']:
        try:
            idx = result[data['name']]['categories'].index(data['category'])
        except:
            pass
        else:
            result[data['name']]['data'][idx] = data['value']  # or 0

    converted_graph_data = {
        'x_axis':  {
            'title': graph_data['x_title'],
            'categories': categories
        },
        'y_axis': {
            'title': graph_data['y_title'],
            'series': [{'name': k, 'data': result[k]['data']} for k in sorted(result.keys())]
        }
    }

    return converted_graph_data


def table_converter(graph_data):
    """
    Converts table data to suitable format for front-end view.
    """
    if not graph_data:
        graph_data['head'] = []
        graph_data['data'] = []
        return graph_data

    for i in range(len(graph_data['head'])):
        if isinstance(graph_data['head'][i], str):
            graph_data['head'][i] = graph_data['head'][i].replace("\\n", "<br>").replace("\t", "&nbsp")

    for i in range(len(graph_data['data'])):
        for j in range(len(graph_data['data'][i])):
            if isinstance(graph_data['data'][i][j], str):
                graph_data['data'][i][j] = graph_data['data'][i][j].replace("\\n", "<br>").replace("\t", "&nbsp")

    return graph_data


# TODO: check and modify it
def chart_converter_3d(graph_data):
    """
    Converts 3D chart data to suitable format for front-end view.
    """
    categories = sorted(list({d['category'].replace("\\n", "<br>").replace("\t", "&nbsp") for d in graph_data['data']}))
    names = sorted(list({d['name'] for d in graph_data['data']}))
    result_paid = {
        name: {
            'categories': categories,
            'data': [0 for i in range(len(categories))],
            'stack': 'paid'
        } for name in names
    }
    result_unpaid = {
        name: {
            'categories': categories,
            'data': [0 for i in range(len(categories))],
            'stack': 'unpaid'
        } for name in names
    }

    for data in graph_data['data']:
        if data['status'] == 'paid':
            try:
                idx = result_paid[data['name']]['categories'].index(data['category'])
            except:
                pass
            else:
                result_paid[data['name']]['data'][idx] = data['value'] or 0
        else:
            try:
                idx = result_unpaid[data['name']]['categories'].index(data['category'])
            except:
                pass
            else:
                result_unpaid[data['name']]['data'][idx] = data['value'] or 0

    converted_graph_data = {
        'x_axis': {
            'title': graph_data['x_title'],
            'categories': categories
        },
        'y_axis': {
            'title': graph_data['y_title'],
            'series': [{'name': v['stack'], 'stack': k, 'data': v['data']} for k, v in result_paid.items()] +
                      [{'name': v['stack'], 'stack': k, 'data': v['data']} for k, v in result_unpaid.items()]

        }
    }

    return converted_graph_data


# TODO: check and modify it
def stacked_horizontal_chart_converter(many_graph_data):
    """
    Converts stacked horizontal chart data to suitable format for front-end view.
    """
    all_categories = []
    series = {}

    for graph_data in many_graph_data['data']:
        parent_category = list(graph_data.keys())[0]
        graph_data = list(graph_data.values())[0]
        categories = sorted(list({d['category'] for d in graph_data}))
        names = sorted(list({d['name'] for d in graph_data}))
        result = {name: {'categories': categories, 'data': [0 for i in range(len(categories))]} for name in names}

        for data in graph_data:
            idx = result[data['name']]['categories'].index(data['category'])
            result[data['name']]['data'][idx] = data['value'] or 0

        all_categories.append(
            {
                "name": parent_category,
                "categories": categories
            }
        )

        for k, v in result.items():
            series[k] = series.get(k, [])
            series[k].extend(v['data'])

    converted_graph_data = {
        'x_axis':  {
            'title': many_graph_data['x_title'],
            'categories': all_categories
        },
        'y_axis': {
            'title': many_graph_data['y_title'],
            'series': series
        }
    }

    return converted_graph_data
