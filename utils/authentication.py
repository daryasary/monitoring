from functools import wraps

from django.http.response import HttpResponseForbidden


def check_login(func):
    """
    A function wrapper to check if user is login.
    """
    @wraps(func)
    def decorator(request, *args, **kwargs):
        print('=====', request.user, '=====')
        if request.user.is_authenticated():
            return func(request, *args, **kwargs)
        else:
            return HttpResponseForbidden()
    return decorator
