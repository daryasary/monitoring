import requests

from django.conf import settings


def send_charge_to_topup(charge):
    data_charge_params = {
        'phone_number': charge.phone_number,
        'package_uuid': charge.package.topup_package,
        'custom_charge': charge.price * 10  # topup uses rial
    }

    try:
        headers = {'Authorization': 'Token %s' % settings.TOPUP_TOKEN}
        r = requests.post('%s/charge/' % settings.TOPUP_URL, headers=headers, data=data_charge_params)
        r.raise_for_status()
        r_json = r.json()
    except requests.exceptions.HTTPError:
        return False, r.content
    except Exception as e:
        return False, str(e)
    else:
        return True, r_json
