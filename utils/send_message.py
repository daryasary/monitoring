import requests

from django.conf import settings


def adp_send(phone_number, msg):
    query_params = {
        'username': settings.ADP_CONFIGS['username'],
        'password': settings.ADP_CONFIGS['password'],
        'srcaddress': settings.ADP_CONFIGS['srcaddress'],
        'unicode': settings.ADP_CONFIGS['unicode'],
        'dstaddress': phone_number,
        'body': msg
    }

    try:
        r = requests.get(url=settings.ADP_CONFIGS['url'], params=query_params, timeout=10)
        r.raise_for_status()
    except Exception as e:
        return False, str(e)
    else:
        return True, r.text


# def mmp_send(phone_number, message):
#     """
#     API to send sms by HamrahVas mmp system
#     :param phone_number:
#     :param message:
#     :return: status and returned message by server
#     """
#     query_params = {
#         'message': message,
#         'scode': settings.HAMRAHVAS_CONFIG['SHORT_CODE'],
#         'mobile': '0' + str(phone_number)[2:],
#         'serviceid': settings.HAMRAHVAS_CONFIG['MMP_ID'],
#         'chargetype': settings.HAMRAHVAS_CONFIG['CHARGE_TYPE'],  # charge_code,
#     }
#
#     try:
#         r = requests.get(
#             "http://%s/mmp/service/send" % settings.HAMRAHVAS_CONFIG['URI_MMP'],
#             params=query_params,
#             timeout=20
#         )
#     except Exception as e:
#         return False, str(e)
#
#     if r.status_code // 100 == 2:
#         return True, r.text
#     else:
#         return False, 'server status code: %s' % r.status_code
#
#
# def sdp_send(phone_number, message):
#     """
#     API to send sms by Irancell sdp system
#     :param phone_number:
#     :param message:
#     :return: status and returned message by server
#     """
#     query_params = {
#         'message': message,
#         'destination': phone_number,
#         'referenceno': settings.MTN_CONFIG['REFERENCE_NO'],
#         'accountId': settings.MTN_CONFIG['SDP_SERVICE_ID'],
#     }
#
#     try:
#         r = requests.get(
#             "http://%s/message/send" % settings.MTN_CONFIG['URI_SDP'],
#             params=query_params,
#             timeout=20
#         )
#     except Exception as e:
#         return False, str(e)
#
#     if r.status_code // 100 == 2:
#         return True, r.text
#     else:
#         return False, 'server status code: %s' % r.status_code
#
#
# def send_email(to):
#     """
#     Send email
#     :param to:
#     :return:
#     """
#     send_mail(
#         subject=_(''),
#         message=_(''),
#         from_email='',
#         recipient_list=[to],
#         fail_silently=True,
#         auth_user='',
#         auth_password='',
#         html_message='',
#     )
