
def generate_report_pattern(title, slug):
    """
    Generate a general pattern for reports
    """
    return {
        'title': title,
        'slug': slug,
        'filters': [],
        'data': [],
        'message': {}
    }


def filter_pattern(slug, filter_type, values):
    """
    Generate a general pattern for filters
    """
    return {
        'filter_slug': slug,
        'filter_type': filter_type,
        'filter_values': values
    }


def table_pattern(title, headers, actions=None):
    """
    Generate a general pattern for tables
    """
    return {
        'graph_title': title,
        'graph_type': 'table',
        'graph_data': {
            'head': headers,
            'data': []
        },
        'actions': actions,
        'summary': []
    }


def text_pattern(title, text='', actions=None):
    """
    Generate a general pattern for text reports
    """
    return {
        'graph_title': title,
        'graph_type': 'text',
        'graph_data': text,
        'actions': actions
    }


def gauge_pattern(title, total, used, order, unit, actions=None):
    """
    Generate a general pattern for gauge charts
    """
    return {
        'graph_title': title,
        'graph_type': 'gauge',
        'graph_data': {
            'total': total,
            'used': used,
            'order': order,
            'unit': unit
        },
        'actions': actions
    }


def chart_pattern(title, graph_type, x_title=None, y_title=None, actions=None):
    """
    Generate a general pattern for charts
    """
    return {
        'graph_title': title,
        'graph_type': graph_type,
        'graph_data': {
            'x_title': x_title,
            'y_title': y_title,
            'definitions': {},
            'data': []
        },
        'actions': actions
    }


def pie_pattern(title, graph_type, name, actions=None):
    """
    Generate a general pattern for pie charts
    """
    return {
        'graph_title': title,
        'graph_type': graph_type,
        'graph_data': {
            'name': name,
            'data': []
        },
        'actions': actions
    }


def form_pattern(title, form_type, form_data=[], actions=None):
    """
    Generate a general pattern for forms
    """
    return {
        'graph_title': title,
        'graph_type': form_type,
        'graph_data': form_data,
        'actions': actions
    }


def json_pattern(title, graph_data={}, actions=None):
    """
    get a dict as graph_data
    """
    return {
        'graph_title': title,
        'graph_type': 'json',
        'graph_data': graph_data,
        'actions': actions
    }
