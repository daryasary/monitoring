from django.conf.urls import url
from django.views.decorators.cache import cache_page

from .views import BulkSMSSendingView, CSVBulkSMSSendingView, \
    saman_bank_daily_reports, saman_bank_total_reports, \
    MobileChargeView, LimitedMobileChargeView

from .views.vas_payment import vas_payment_price_sum_hourly, incomes_from_joined_users, status_count, daily_incomes


urlpatterns = [
    # SMS Sending (forms)
    url(r'^bulk-sms-sending/$', BulkSMSSendingView.as_view()),
    url(r'^bulk-sms-sending-csv/$', CSVBulkSMSSendingView.as_view()),

    # Bank Reports
    url(r'^saman-bank-daily/$', saman_bank_daily_reports),
    url(r'^saman-bank-total/$', saman_bank_total_reports),

    # Mobile Charge
    url(r'^mobile-charge/$', MobileChargeView.as_view()),
    url(r'^mobile-charge-limited/$', LimitedMobileChargeView.as_view()),

    # TODO: delete these reports after vas_payment upgraded
    # Vas Payment Reports
    url(r'^vas_payment_price_sum_hourly/$', vas_payment_price_sum_hourly),
    url(r'^incomes_from_joined_users/$', incomes_from_joined_users),
    url(r'^status_count/$', status_count),
    url(r'^daily_incomes/$', daily_incomes),
]
