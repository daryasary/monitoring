import requests
import logging
from urllib.parse import urljoin
from pymongo import MongoClient

from django.core.management.base import BaseCommand
from django.conf import settings
from django.utils import timezone


logger = logging.getLogger('vas_payment')


class Command(BaseCommand):
    help = 'Save price sum of vas payments hourly.'

    def handle(self, *args, **options):
        client = MongoClient(settings.MONGO_HOST)
        client[settings.MONGO_DB].authenticate(settings.MONGO_USER, settings.MONGO_PASS, mechanism='MONGODB-CR')
        db = client[settings.MONGO_DB]
        collection = db['vas_payment_price_sum_hourly']

        url = urljoin(settings.VAS_PAYMENT_URL, '/reports_new/finance/payments_log_price_sum/')

        for i in range(3):
            try:
                r = requests.get(url=url, timeout=60 * 3)
                r_json = r.json()
            except Exception as e:
                logger.info('[%s]-[vas-payment-incomes-hourly] Error: %s' % (str(timezone.now()), str(e)))
            else:
                now = timezone.now()
                collection.insert_one(
                    {
                        'created_time': now,
                        'date': str(now.date()),
                        'time': now.hour,
                        'price_sum': r_json['price_sum']
                    }
                )
                break

