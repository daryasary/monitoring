import io
import csv
import logging
from ast import literal_eval

from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication

from utils.reports_patterns_generator import generate_report_pattern, form_pattern, text_pattern
from reports.tasks import send_bulk_sms


User = get_user_model()
logger_send_sms = logging.getLogger('sms_send')


class BulkSMSSendingView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        bulk_sms = generate_report_pattern('ارسال دسته جمعی پیامک', 'bulk_sms')
        bulk_sms_text = text_pattern('', 'لطفا شماره تلفن ها را با ۹۸ شروع کنید و با زدن Enter از هم جدا کنید.')
        bulk_sms_form = form_pattern('', 'form')
        bulk_sms_form['graph_data'] = [
            {
                'label': 'متن پیام',
                'type': 'textarea',
                'name': 'message',
                'required': True,
                'cols': 80,
                'rows': 10
            },
            {
                'label': 'شماره موبایل ها',
                'type': 'list',
                'name': 'phone_numbers',
                'required': True,
                'cols': 20
            }
        ]

        bulk_sms['data'].append(bulk_sms_text)
        bulk_sms['data'].append(bulk_sms_form)

        return Response(bulk_sms)

    def post(self, request):
        user_id = request.data.get('user_id', None)
        message = request.data.get('message', None)
        phone_numbers = request.data.get('phone_numbers', None)

        try:
            user = User.objects.get(id=user_id, is_active=True)
            phone_numbers = literal_eval(phone_numbers)
            phone_numbers = set(phone_numbers)
        except User.DoesNotExist:
            msg = _('User is deactivate.')
            logger_send_sms.error('[%s][%s][%s][%s]' % (msg, user_id, phone_numbers, message))
            return Response({'detail': msg}, status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            msg = _(str(e))
            logger_send_sms.error('[%s][%s][%s][%s]' % (msg, user_id, phone_numbers, message))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        for phone_number in phone_numbers:
            send_bulk_sms.delay(user.id, phone_number, message)
            logger_send_sms.info('[OK][%s][%s][%s]' % (user.id, phone_number, message))

        return Response({'detail': _('SMS are sent.')}, status=status.HTTP_200_OK)


class CSVBulkSMSSendingView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        bulk_sms_csv = generate_report_pattern('ارسال دسته جمعی پیامک با فایل CSV', 'bulk-sms-csv')
        bulk_sms_csv_form = form_pattern('', 'form')
        bulk_sms_csv_form['graph_data'] = [
            {
                'label': 'فایل CSV',
                'type': 'file',
                'name': 'csv_file',
                'required': True,
                'help': 'لطفا فایل CSV که حاوی پیام ها و شماره تلفن ها می باشد را آپلود کنید.'
            },
        ]

        bulk_sms_csv['data'].append(bulk_sms_csv_form)

        return Response(bulk_sms_csv)

    def post(self, request):
        user_id = request.data.get('user_id', None)
        csv_file = request.FILES.get('csv_file', None)

        try:
            user = User.objects.get(id=user_id, is_active=True)
            decoded_file = csv_file.read().decode('utf-8')
            io_string = io.StringIO(decoded_file)
            csv_data = csv.DictReader(io_string)
        except User.DoesNotExist:
            msg = _('User is deactivate.')
            logger_send_sms.error('[%s][%s][%s]' % (msg, user_id, csv_file))
            return Response({'detail': msg}, status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            msg = _(str(e))
            logger_send_sms.error('[%s][%s][%s][%s]' % (msg, user_id, csv_file, str(e)))
            return Response({'detail': _('Wrong CSV File')}, status=status.HTTP_400_BAD_REQUEST)

        try:
            for row in csv_data:
                send_bulk_sms.delay(user.id, row['phone_number'], row['message'])
                logger_send_sms.info('[OK][%s][%s][%s]' % (user.id, row['phone_number'], row['message']))
        except Exception as e:
            msg = _('Wrong CSV Data')
            logger_send_sms.error('[%s][%s][%s][%s]' % (msg, user_id, csv_file, str(e)))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        return Response({'detail': _('SMS are sent.')}, status=status.HTTP_200_OK)


# old versions

# @api_view(['GET', 'POST'])
# @authentication_classes((TokenAuthentication, ))
# @permission_classes((IsAuthenticated, ))
# def bulk_sms_sending(request):
#     """
#     A report of type form to send a message to many phone numbers.
#     User could select an operator for sms sending.
#     """
#
#     if request.method == 'GET':
#         bulk_sms = generate_report_pattern('ارسال دسته جمعی پیامک', 'bulk_sms')
#         bulk_sms_text = text_pattern('', 'لطفا شماره تلفن ها را با ۹۸ شروع کنید و با زدن Enter از هم جدا کنید.')
#         bulk_sms_form = form_pattern('', 'form')
#         bulk_sms_form['graph_data'] = [
#             {
#                 'label': 'متن پیام',
#                 'type': 'textarea',
#                 'name': 'message',
#                 'required': True,
#                 'cols': 80,
#                 'rows': 10
#             },
#             {
#                 'label': 'شماره موبایل ها',
#                 'type': 'list',
#                 'name': 'phone_numbers',
#                 'required': True,
#                 'cols': 20
#             }
#         ]
#         bulk_sms['data'].append(bulk_sms_text)
#         bulk_sms['data'].append(bulk_sms_form)
#
#         return Response(bulk_sms)
#
#     elif request.method == 'POST':
#         user_id = request.data.get('user_id', None)
#         message = request.data.get('message', None)
#         # phone_numbers = request.data.getlist('phone_numbers')
#         phone_numbers = request.data.get('phone_numbers', None)
#
#         if not (user_id or message or phone_numbers):
#             return Response({'detail': _('Incomplete data')}, status=status.HTTP_400_BAD_REQUEST)
#
#         try:
#             phone_numbers = literal_eval(phone_numbers)
#         except:
#             return Response({'detail': _('Wrong Phone Numbers')}, status=status.HTTP_400_BAD_REQUEST)
#
#         phone_numbers = set(phone_numbers)
#         all_sms_status = []
#
#         for phone_number in phone_numbers:
#             sms_status, sms_msg = adp_send(phone_number, message)
#             all_sms_status.append(sms_status)
#
#             SMSSendLog.objects.create(
#                 user_id=user_id,
#                 message=message,
#                 phone_number=phone_number,
#                 send_status=sms_status,
#                 description=sms_msg
#             )
#
#
# @api_view(['GET', 'POST'])
# @authentication_classes((TokenAuthentication, ))
# @permission_classes((IsAuthenticated, ))
# def bulk_sms_sending_csv(request):
#     """
#     A report of type form to read a csv file and send a specific message to each phone number.
#     User could select an operator for sms sending.
#     """
#
#     if request.method == 'GET':
#         bulk_sms_csv = generate_report_pattern('ارسال دسته جمعی پیامک با فایل CSV', 'bulk-sms-csv')
#         bulk_sms_csv_form = form_pattern('', 'form')
#         bulk_sms_csv_form['graph_data'] = [
#             {
#                 'label': 'فایل CSV',
#                 'type': 'file',
#                 'name': 'csv_file',
#                 'required': True,
#                 'help': 'لطفا فایل CSV که حاوی پیام ها و شماره تلفن ها می باشد را آپلود کنید.'
#             },
#         ]
#         bulk_sms_csv['data'].append(bulk_sms_csv_form)
#
#         return Response(bulk_sms_csv)
#
#     elif request.method == 'POST':
#         user_id = request.data.get('user_id', None)
#         csv_file = request.FILES.get('csv_file')
#
#         if not (user_id or csv_file):
#             return Response({'detail': _('Incomplete data')}, status=status.HTTP_400_BAD_REQUEST)
#         if csv_file.size == 0:
#             return Response({'detail': _('File is empty')}, status=status.HTTP_400_BAD_REQUEST)
#
#         try:
#             decoded_file = csv_file.read().decode('utf-8')
#             io_string = io.StringIO(decoded_file)
#             csv_data = csv.DictReader(io_string)
#         except:
#             return Response({'detail': _('Wrong CSV File')}, status=status.HTTP_400_BAD_REQUEST)
#
#         all_sms_status = []
#
#         try:
#             for row in csv_data:
#                 sms_status, sms_msg = adp_send(row['phone_number'], row['message'])
#                 all_sms_status.append(sms_status)
#
#                 SMSSendLog.objects.create(
#                     user_id=user_id,
#                     message=row['message'],
#                     phone_number=row['phone_number'],
#                     send_status=sms_status,
#                     description=sms_msg
#                 )
#         except:
#             return Response({'detail': _('Wrong CSV Data')}, status=status.HTTP_400_BAD_REQUEST)
#
# import csv
# import io
# from ast import literal_eval
#
# from django.utils.translation import ugettext as _
#
# from rest_framework import status
# from rest_framework.response import Response
# from rest_framework.authentication import TokenAuthentication
# from rest_framework.views import APIView
#
# from utils.reports_patterns_generator import generate_report_pattern, form_pattern, text_pattern
# from utils.send_message import adp_send, mmp_send, sdp_send
# from service.models import SMSSendLog
#
#
# OPERATOR_SELECT_VALUES = [
#     {'text': 'همراه اول', 'value': 'hamrahaval'},  # mmp
#     {'text': 'ایرانسل', 'value': 'irancell'},  # sdp
# ]
#
# OPERATOR_SEND_METHOD = {
#     'hamrahaval': mmp_send,
#     'irancell': sdp_send,
# }
#
#
# class BulkSMS(APIView):
#     """
#     A report of type form to send a message to many phone numbers.
#     User could select an operator for sms sending.
#     """
#     authentication_classes = (TokenAuthentication, )
#
#     def get(self, request):
#         bulk_sms = generate_report_pattern('ارسال دسته جمعی پیامک', 'bulk_sms')
#         bulk_sms_text = text_pattern('', 'لطفا شماره تلفن ها را با ۹۸ شروع کنید و با زدن ز هم جدا کنید.')
#         bulk_sms_form = form_pattern('', 'form')
#         bulk_sms_form['graph_data'] = [
#             {
#                 'label': 'متن پیام',
#                 'type': 'textarea',
#                 'name': 'message',
#                 'required': True,
#                 'cols': 80,
#                 'rows': 10
#             },
#             {
#                 'label': 'شماره موبایل ها',
#                 'type': 'list',
#                 'name': 'phone_numbers',
#                 'required': True,
#                 'cols': 20
#             },
#             {
#                 'label': 'اپراتور',
#                 'type': 'dropdown',
#                 'name': 'operator',
#                 'required': True,
#                 'values': OPERATOR_SELECT_VALUES
#             }
#         ]
#         bulk_sms['data'].append(bulk_sms_text)
#         bulk_sms['data'].append(bulk_sms_form)
#         return Response(bulk_sms)
#
#     def post(self, request):
#         user_id = request.data.get('user_id', None)
#         message = request.data.get('message', None)
#         # phone_numbers = request.data.getlist('phone_numbers')
#         phone_numbers = request.data.get('phone_numbers', None)
#         operator = request.data.get('operator', None)
#
#         if not (user_id or message or phone_numbers or operator):
#             return Response({'detail': _('Incomplete data')}, status=status.HTTP_400_BAD_REQUEST)
#
#         try:
#             phone_numbers = literal_eval(phone_numbers)
#         except:
#             return Response({'detail': _('Wrong Phone Numbers')}, status=status.HTTP_400_BAD_REQUEST)
#         else:
#             phone_numbers = set(phone_numbers)
#
#         send_sms_method = OPERATOR_SEND_METHOD[operator]
#         all_sms_status = []
#
#         for phone_number in phone_numbers:
#             sms_status, sms_msg = send_sms_method(phone_number, message)
#             all_sms_status.append(sms_status)
#
#             SMSSendLog(
#                 user_id=user_id,
#                 message=message,
#                 phone_number=phone_number,
#                 send_status=sms_status,
#                 description=sms_msg,
#                 operator=operator
#             ).save()
#
#
# class BulkSMSCSV(APIView):
#     """
#     A report of type form to read a csv file and send a specific message to each phone number.
#     User could select an operator for sms sending.
#     """
#     authentication_classes = (TokenAuthentication, )
#
#     def get(self, request):
#         bulk_sms_csv = generate_report_pattern('ارسال دسته جمعی پیامک با فایل CSV', 'bulk-sms-csv')
#         bulk_sms_csv_form = form_pattern('', 'form')
#         bulk_sms_csv_form['graph_data'] = [
#             {
#                 'label': 'فایل CSV',
#                 'type': 'file',
#                 'name': 'csv_file',
#                 'required': True,
#                 'help': 'لطفا فایل CSV که حاوی پیام ها و شماره تلفن ها می باشد را آپلود کنید.'
#             },
#             {
#                 'label': 'اپراتور',
#                 'type': 'dropdown',
#                 'name': 'operator',
#                 'required': True,
#                 'values': OPERATOR_SELECT_VALUES
#             }
#         ]
#         bulk_sms_csv['data'].append(bulk_sms_csv_form)
#         return Response(bulk_sms_csv)
#
#     def post(self, request):
#         user_id = request.data.get('user_id', None)
#         csv_file = request.FILES.get('csv_file')
#         operator = request.data.get('operator', None)
#
#         if not (user_id or csv_file or operator):
#             return Response({'detail': _('Incomplete data')}, status=status.HTTP_400_BAD_REQUEST)
#         if csv_file.size == 0:
#             return Response({'detail': _('File is empty')}, status=status.HTTP_400_BAD_REQUEST)
#
#         try:
#             decoded_file = csv_file.read().decode('utf-8')
#             io_string = io.StringIO(decoded_file)
#             csv_data = csv.DictReader(io_string)
#         except:
#             return Response({'detail': _('Wrong CSV File')}, status=status.HTTP_400_BAD_REQUEST)
#
#         send_sms_method = OPERATOR_SEND_METHOD[operator]
#         all_sms_status = []
#
#         try:
#             for row in csv_data:
#                 sms_status, sms_msg = send_sms_method(row['phone_number'], row['message'])
#                 all_sms_status.append(sms_status)
#
#                 SMSSendLog(
#                     user_id=user_id,
#                     message=row['message'],
#                     phone_number=row['phone_number'],
#                     send_status=sms_status,
#                     description=sms_msg,
#                     operator=operator
#                 ).save()
#
#         except:
#             return Response({'detail': _('Wrong CSV Data')}, status=status.HTTP_400_BAD_REQUEST)
#
