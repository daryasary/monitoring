import requests
from pymongo import MongoClient
from khayyam import JalaliDate

from django.utils import timezone
from django.conf import settings

from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, authentication_classes, permission_classes

from utils.reports_patterns_generator import generate_report_pattern, chart_pattern, filter_pattern, text_pattern


AGGREGATOR_FILTER = [
    {'text': 'Hamrah VAS', 'value': 'hamrahvas'},
    {'text': 'MCI Hamrah', 'value': 'mcihamrah'},
    {'text': 'MTN Irancell', 'value': 'mtnirancell'},
    {'text': 'Tel Com Iran', 'value': 'telecomiran'}
]

COLORS = [
    '#641E16', '#CD6155', '#512E5F', '#BB8FCE', '#1B4F72', '#5499C7', '#0E6251', '#145A32', '#82E0AA', '#ABEBC6',
    '#7D6608', '#F4D03F', '#F39C12', '#6E2C00', '#EDBB99', '#7B7D7D', '#616A6B', '#5D6D7E', '#17202A', '#FF0000',
    '#FFFF00', '#808000', '#00FF00', '#008000', '#00FFFF', '#008080', '#0000FF', '#000080', '#FF00FF', '#800080'
]


@api_view(['GET', 'POST'])
@authentication_classes((TokenAuthentication, ))
@permission_classes((IsAuthenticated, ))
def vas_payment_price_sum_hourly(request):
    """
    Report for hourly incomes of vas payment.
    """
    start_date = request.data.get('start_date', None)
    end_date = request.data.get('end_date', None)

    client = MongoClient(settings.MONGO_HOST)
    client[settings.MONGO_DB].authenticate(settings.MONGO_USER, settings.MONGO_PASS, mechanism='MONGODB-CR')
    db = client[settings.MONGO_DB]
    collection = db['vas_payment_price_sum_hourly']

    if start_date:
        start_date = timezone.datetime.strptime(start_date, '%Y-%m-%d')
    else:
        start_date = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0) - timezone.timedelta(days=7)

    if end_date:
        end_date = timezone.datetime.strptime(end_date, '%Y-%m-%d')
    else:
        end_date = timezone.now()

    prices = list(collection.find(
        {
            'date': {
                '$gte': start_date.strftime('%Y-%m-%d'),
                '$lte': end_date.strftime('%Y-%m-%d')
                # '$gte': timezone.datetime.strptime(start_date, '%Y-%m-%d') if start_date else
                # timezone.now().replace(hour=0, minute=0, second=0, microsecond=0) - timezone.timedelta(days=4),
                # '$lte': timezone.datetime.strptime(end_date, '%Y-%m-%d') if end_date else timezone.now()
            }
        }
    ))

    report = generate_report_pattern('درآمد ساعتی سیستم VAS', 'vas_payment_price_sum_hourly')
    report['filters'].append(filter_pattern('date', 'date_hijri', ['start_date', 'end_date']))
    prices_status_chart = chart_pattern('درآمد ساعتی', 'chart-linear', 'Hours', 'Prices')
    campaign_prices_chart = chart_pattern('درآمد ساعتی به تفکیک کمپین', 'chart-linear', 'Hours', 'Prices')

    for p in prices:
        p_date = timezone.datetime.strptime(p['date'], '%Y-%m-%d').date()
        prices_status_chart['graph_data']['data'].append(
            {
                'category': p['time'],
                'name': '%s (%s)' % (JalaliDate(p_date).strftime('%m-%d'), p_date.strftime('%b-%d')),
                'value': p['price_sum'] if p['price_sum'] != 0 else None
            }
        )

    collection = db['vas_payment_price_sum_per_campaign_hourly']
    campaign_prices = list(collection.find({'date': str(timezone.now().date())}))

    for cp in campaign_prices:
        campaign_prices_chart['graph_data']['data'].append(
            {
                'category': cp['time'],
                'name': cp['campaign_title'] if cp['campaign_title'] else 'بدون کمپین',
                'value': cp['price_sum']
            }
        )

    report['data'].append(prices_status_chart)
    report['data'].append(campaign_prices_chart)

    return Response(report)


@api_view(['GET', 'POST'])
@authentication_classes((TokenAuthentication, ))
@permission_classes((IsAuthenticated, ))
def incomes_from_joined_users(request):
    start_date = request.data.get('start_date', None)
    end_date = request.data.get('end_date', None)

    if start_date and end_date:
        days_to_view = (timezone.datetime.strptime(end_date, '%Y-%m-%d') - timezone.datetime.strptime(start_date, '%Y-%m-%d')).days
    else:
        days_to_view = 10

    start_date = timezone.datetime.strptime(start_date, '%Y-%m-%d') if start_date else timezone.now().replace(hour=0, minute=0, second=0, microsecond=0) - timezone.timedelta(days=30)
    end_date = timezone.datetime.strptime(end_date, '%Y-%m-%d') if end_date else timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
    end_date += timezone.timedelta(days=1)

    client = MongoClient(settings.MONGO_HOST)
    client[settings.MONGO_DB].authenticate(settings.MONGO_USER, settings.MONGO_PASS, mechanism='MONGODB-CR')
    db = client[settings.MONGO_DB]

    filter_params = {
        'date_joined': {
            '$gte': start_date,
            '$lte': end_date
        }
    }

    collection = db['vas_payment_incomes_from_joined_users_daily_p']
    p_results = list(collection.find(filter_params))

    collection = db['vas_payment_incomes_from_joined_users_daily_pl']
    pl_results = list(collection.find(filter_params))

    report = generate_report_pattern('وضعیت بسته ها و شارژینگ', 'payments_stats')
    report['filters'].append(filter_pattern('date', 'date_hijri', ['start_date', 'end_date']))

    days = [str(JalaliDate(start_date + timezone.timedelta(days=i))) for i in range(0, (end_date - start_date).days - 1)]

    series_pl_total_charged = []
    series_pl_payment_count = []
    series_p_count = []
    none_list = [None for d in days]

    for day in days:
        sd_pl_total_charge = none_list.copy()
        sd_pl_payment_count = none_list.copy()
        sd_p_count = none_list.copy()

        for res in pl_results:
            j_date_joined = str(JalaliDate(res['date_joined'].date()))
            j_date_charged = str(JalaliDate(res['date_charged'].date()))
            if j_date_joined == day and j_date_charged in days:
                sd_pl_total_charge[days.index(j_date_charged)] = res['total_charged']
                sd_pl_payment_count[days.index(j_date_charged)] = res['payment_count']

        series_pl_total_charged.append(
            {
                # 'name': day,
                'name': '%s (%s)' % (JalaliDate.strptime(day, '%Y-%m-%d').strftime('%m-%d'),
                                     JalaliDate.strptime(day, '%Y-%m-%d').todate().strftime('%b-%d')),
                'data': sd_pl_total_charge[-days_to_view:],
                'color': COLORS[days.index(day) % len(COLORS)]
            }
        )

        series_pl_payment_count.append(
            {
                # 'name': day,
                'name': '%s (%s)' % (JalaliDate.strptime(day, '%Y-%m-%d').strftime('%m-%d'),
                                     JalaliDate.strptime(day, '%Y-%m-%d').todate().strftime('%b-%d')),
                'data': sd_pl_payment_count[-days_to_view:],
                'color': COLORS[days.index(day) % len(COLORS)]
            }
        )

        for res in p_results:
            j_date_joined = str(JalaliDate(res['date_joined'].date()))
            j_date_counted = str(JalaliDate(res['date_counted'].date()))
            if j_date_joined == day and j_date_counted in days:
                sd_p_count[days.index(j_date_counted)] = res['p_count']

        series_p_count.append(
            {
                # 'name': day,
                'name': '%s (%s)' % (JalaliDate.strptime(day, '%Y-%m-%d').strftime('%m-%d'),
                                     JalaliDate.strptime(day, '%Y-%m-%d').todate().strftime('%b-%d')),
                'data': sd_p_count[-days_to_view:],
                'color': COLORS[days.index(day) % len(COLORS)]
            }
        )

    payment_logs_total_charge_chart = {
        'graph_title': ' (is_renew=0) میزان شارژینگ به تفکیک تاریخ عضویت',
        'graph_type': 'chart-linear-custom',
        'graph_data': {
            'x_axis': {
                'categories': days[-days_to_view:],
                'title': 'Days'
            },
            'y_axis': {
                'title': 'Charge',
                'series': series_pl_total_charged
            }
        }
    }

    payment_logs_payment_count_chart = {
        'graph_title': ' (is_renew=0) تعداد شارژینگ به تفکیک تاریخ عضویت',
        'graph_type': 'chart-linear-custom',
        'graph_data': {
            'x_axis': {
                'categories': days[-days_to_view:],
                'title': 'Days'
            },
            'y_axis': {
                'title': 'Count',
                'series': series_pl_payment_count
            }
        }
    }

    payment_count_chart = {
        'graph_title': ' (is_renew=0) تعداد بسته های فعال به تفکیک تاریخ عضویت',
        'graph_type': 'chart-linear-custom',
        'graph_data': {
            'x_axis': {
                'categories': days[-days_to_view:],
                'title': 'Days'
            },
            'y_axis': {
                'title': 'Count',
                'series': series_p_count
            }
        }
    }

    report['data'].append(payment_logs_total_charge_chart)
    report['data'].append(payment_logs_payment_count_chart)
    report['data'].append(payment_count_chart)

    return Response(report)


@api_view(['GET', 'POST'])
@authentication_classes((TokenAuthentication, ))
@permission_classes((IsAuthenticated, ))
def status_count(request):
    start_date = request.data.get('start_date', None)
    end_date = request.data.get('end_date', None)
    aggregator = request.data.get('aggregator', None)

    start_date = timezone.datetime.strptime(start_date, '%Y-%m-%d') if start_date else timezone.now().replace(hour=0, minute=0, second=0) - timezone.timedelta(days=10)
    end_date = timezone.datetime.strptime(end_date, '%Y-%m-%d') if end_date else timezone.now()

    client = MongoClient(settings.MONGO_HOST)
    client[settings.MONGO_DB].authenticate(settings.MONGO_USER, settings.MONGO_PASS, mechanism='MONGODB-CR')
    db = client[settings.MONGO_DB]

    collection = db['vas_payment_status_count_hourly']
    payment_status_results_today = list(collection.find({'date': str(timezone.now().date())}))

    collection = db['vas_payment_status_count_daily']
    payment_status_results_daily = list(collection.find(
        {
            'date_canceled': {
                '$gte': start_date,  # .strftime('%Y-%m-%d'),
                '$lte': end_date  # .strftime('%Y-%m-%d')
            }
        }
    ))

    report = generate_report_pattern('وضعیت بسته ها (Payment Status)', 'vas_payment_debt_cancel_hourly')
    report['filters'].append(filter_pattern('date', 'date_hijri', ['start_date', 'end_date']))
    report['filters'].append(filter_pattern('aggregator', 'dropdown', AGGREGATOR_FILTER))

    today_debt_status_chart = chart_pattern('تعداد بسته های فعال به تفکیک ساعت', 'chart-linear', 'Hours', 'Count')
    debt_status_chart = chart_pattern('بسته های فعال و شارژ شده در هر روز', 'chart-linear', 'Days', 'Count')
    cancel_status_chart = chart_pattern('عضویت و لغو روزانه', 'chart-linear', 'Days', 'Count')

    for p in payment_status_results_today:
        if aggregator:
            debt = p['debt'].get(aggregator, 0)
            # cancel = p['cancel'].get(aggregator, 0)
        else:
            debt = sum(p['debt'].values())
            # cancel = sum(p['cancel'].values())

        today_debt_status_chart['graph_data']['data'].append(
            {
                'category': p['time'],  # str(p['date'].date()),
                'name': 'debt',
                'value': debt or None
            }
        )

    for p in payment_status_results_daily:
        if aggregator:
            debt = p['debt'].get(aggregator, 0)
            cancel = p['cancel'].get(aggregator, 0)
            joined = p['joined'].get(aggregator, 0) if p.get('joined') else None
        else:
            debt = sum(p['debt'].values())
            cancel = sum(p['cancel'].values())
            joined = sum(p['joined'].values()) if p.get('joined') else None

        debt_status_chart['graph_data']['data'].append(
            {
                'category': str(JalaliDate(p['date_canceled'])),
                'name': 'total payments',
                'value': debt or None
            }
        )
        debt_status_chart['graph_data']['data'].append(
            {
                'category': str(JalaliDate(p['date_canceled'])),
                'name': 'charged payments',
                'value': p.get('charged', None)
            }
        )

        cancel_status_chart['graph_data']['data'].append(
            {
                'category': str(JalaliDate(p['date_canceled'])),
                'name': 'cancel',
                'value': cancel or None
            }
        )
        cancel_status_chart['graph_data']['data'].append(
            {
                'category': str(JalaliDate(p['date_canceled'])),
                'name': 'joined',
                'value': joined or None
            }
        )

    report['data'].append(today_debt_status_chart)
    report['data'].append(debt_status_chart)
    report['data'].append(cancel_status_chart)

    return Response(report)


@api_view(['GET', 'POST'])
@authentication_classes((TokenAuthentication, ))
@permission_classes((IsAuthenticated, ))
def daily_incomes(request):
    start_date = request.data.get('start_date', None)
    end_date = request.data.get('end_date', None)
    service = request.data.get('service', None)
    operator = request.data.get('operator', None)

    start_date = timezone.datetime.strptime(start_date, '%Y-%m-%d') if start_date else timezone.now() - timezone.timedelta(days=30)
    end_date = timezone.datetime.strptime(end_date, '%Y-%m-%d') if end_date else timezone.now()

    filter_params = {
        'date': {
            '$gte': start_date,
            '$lte': end_date
        }
    }

    operator_filters = [
        {'value': 'hamrahvas', 'text': 'Hamrah VAS'},
        {'value': 'mcihamrah', 'text': 'MCI Hamrah'},
        {'value': 'mtnirancell', 'text': 'MTN Irancell'},
        {'value': 'telecomiran', 'text': 'Tel Com Iran'}
    ]

    report = generate_report_pattern('درآمد روزانه', 'daily_incomes')
    report['filters'].append(filter_pattern('date', 'date_hijri', ['start_date', 'end_date']))
    report['filters'].append(filter_pattern('operator', 'dropdown', operator_filters))

    try:
        r = requests.get('http://vasp.yaramobile.com/reports_new/get_services_list/', timeout=10)
        service_filters = r.json()['service_filters']
    except:
        pass
    else:
        report['filters'].append(filter_pattern('service', 'dropdown', service_filters))

    daily_incomes_linear_chart = chart_pattern('درآمد روزانه', 'chart-linear', 'Dates', 'Amounts')
    daily_incomes_text = text_pattern('درآمد کل')

    client = MongoClient(settings.MONGO_HOST)
    client[settings.MONGO_DB].authenticate(settings.MONGO_USER, settings.MONGO_PASS, mechanism='MONGODB-CR')
    db = client[settings.MONGO_DB]
    collection = db['vas_payment_incomes_daily']
    incomes = list(collection.find(filter_params))

    total_amount = 0
    for inc in incomes:
        amount = inc['total']['total']

        try:
            if operator and service:
                amount = inc[operator][str(service)]
            if operator and not service:
                amount = inc[operator]['total']
            if not operator and service:
                amount = inc['total'][str(service)]
        except:
            amount = 0

        daily_incomes_linear_chart['graph_data']['data'].append(
            {
                'category': str(JalaliDate(inc['date'].date())),
                'name': 'incomes',
                'value': amount
            }
        )

        total_amount += amount

    daily_incomes_text['graph_data'] = total_amount

    report['data'].append(daily_incomes_linear_chart)
    report['data'].append(daily_incomes_text)

    return Response(report)
