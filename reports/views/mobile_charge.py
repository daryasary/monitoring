import logging

from django.utils import timezone
from django.db.models import Sum
from django.db.models.functions import Coalesce
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication

from utils.reports_patterns_generator import generate_report_pattern, form_pattern, text_pattern
from utils.validators import phone_number_validator
from mobile_charge.models import Package, ChargeLog
from reports.tasks import send_mobile_charge_to_topup


User = get_user_model()
logger_mobile_charge = logging.getLogger('mobile_charge')

PACKAGES = [{'value': p.id, 'text': p.title} for p in Package.live.order_by('-id')]


class MobileChargeView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        mobile_charge = generate_report_pattern('Mobile Charge Form', 'mobile-charge-form')
        mobile_charge_text = text_pattern('', 'لطفا شماره تلفن ها را با ۹۸ شروع کنید.')
        mobile_charge_form = form_pattern('', 'form')
        mobile_charge_form['graph_data'] = [
            {
                'label': 'بسته ها',
                'type': 'dropdown',
                'name': 'package',
                'required': True,
                'values': PACKAGES
            },
            {
                'label': 'شماره موبایل',
                'type': 'text',
                'name': 'phone_number',
                'required': True,
                'cols': 15
            }
        ]

        mobile_charge['data'].append(mobile_charge_form)
        mobile_charge['data'].append(mobile_charge_text)

        return Response(mobile_charge)

    def post(self, request):
        user_id = request.data.get('user_id', None)
        phone_number = request.data.get('phone_number', None)
        package_id = request.data.get('package', None)

        try:
            user = User.objects.get(id=user_id, is_active=True)
            package = Package.live.get(pk=package_id)
            phone_number = int(phone_number)
            phone_number_validator(phone_number)
        except User.DoesNotExist:
            msg = _('User is deactivate.')
            logger_mobile_charge.error('[%s][%s][%s][%s]' % (msg, user_id, phone_number, package_id))
            return Response({'detail': msg}, status=status.HTTP_401_UNAUTHORIZED)
        except Package.DoesNotExist:
            msg = _('Package not found')
            logger_mobile_charge.error('[%s][%s][%s][%s]' % (msg, user_id, phone_number, package_id))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            msg = _(str(e))
            logger_mobile_charge.error('[%s][%s][%s][%s]' % (msg, user_id, phone_number, package_id))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        charge = ChargeLog.objects.create(
            user=user,
            package=package,
            phone_number=phone_number,
            price=package.price,
            status=ChargeLog.CHARGE_STATUS_WAITING
        )

        send_mobile_charge_to_topup.delay(charge.id)

        msg = _('Request send to topup')
        logger_mobile_charge.info('[%s][%s][%s][%s]' % (msg, user_id, phone_number, package_id))

        return Response({'detail': msg})


class LimitedMobileChargeView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        mobile_charge = generate_report_pattern('Mobile Charge Form', 'mobile-charge-form')
        mobile_charge_text = text_pattern('', 'لطفا شماره تلفن ها را با ۹۸ شروع کنید.')
        mobile_charge_form = form_pattern('', 'form')
        mobile_charge_form['graph_data'] = [
            {
                'label': 'بسته ها',
                'type': 'dropdown',
                'name': 'package',
                'required': True,
                'values': PACKAGES
            },
            {
                'label': 'شماره موبایل',
                'type': 'text',
                'name': 'phone_number',
                'required': True,
                'cols': 15
            }
        ]

        mobile_charge['data'].append(mobile_charge_form)
        mobile_charge['data'].append(mobile_charge_text)

        return Response(mobile_charge)

    def post(self, request):
        user_id = request.data.get('user_id', 0)
        phone_number = request.data.get('phone_number', None)
        package_id = request.data.get('package', None)

        try:
            user = User.objects.get(id=user_id, is_active=True)
            charge_limit = user.user_charge_limit.charge_limit
            package = Package.live.get(pk=package_id)
            phone_number = int(phone_number)
            phone_number_validator(phone_number)
        except User.DoesNotExist:
            msg = _('User is deactivate.')
            logger_mobile_charge.error('[%s][%s][%s][%s]' % (msg, user_id, phone_number, package_id))
            return Response({'detail': msg}, status=status.HTTP_401_UNAUTHORIZED)
        except Package.DoesNotExist:
            msg = _('Package not found')
            logger_mobile_charge.error('[%s][%s][%s][%s]' % (msg, user_id, phone_number, package_id))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            msg = _(str(e))
            logger_mobile_charge.error('[%s][%s][%s][%s]' % (msg, user_id, phone_number, package_id))
            return Response({'detail': msg}, status=status.HTTP_400_BAD_REQUEST)

        today_charges_sum = ChargeLog.objects.filter(
            user=user,
            created_time__gte=timezone.now().date(),
            status=ChargeLog.CHARGE_STATUS_SUCCESSFUL
        ).aggregate(price_sum=Coalesce(Sum('price'), 0))['price_sum']

        if (today_charges_sum + package.price) > charge_limit:
            msg = _('User charge limit is exceeded for today')
            logger_mobile_charge.error('[%s][%s][%s][%s]' % (msg, user_id, phone_number, package_id))
            return Response({'detail': msg})

        charge = ChargeLog.objects.create(
            user=user,
            package=package,
            phone_number=phone_number,
            price=package.price,
            status=ChargeLog.CHARGE_STATUS_WAITING
        )

        send_mobile_charge_to_topup.delay(charge.id)

        msg = _('Request send to topup')
        logger_mobile_charge.info('[%s][%s][%s][%s]' % (msg, user_id, phone_number, package_id))

        return Response({'detail': msg})
