from celery import shared_task
# from celery.schedules import crontab
# from celery.task import periodic_task

from service.models import SMSSendLog
from mobile_charge.models import ChargeLog
from utils.send_message import adp_send
from utils.topup import send_charge_to_topup


@shared_task
def send_bulk_sms(user_id, phone_number, msg):
    sms_status, sms_response = adp_send(phone_number, msg)
    SMSSendLog.objects.create(
        user_id=user_id,
        message=msg,
        phone_number=phone_number,
        send_status=sms_status,
        description=sms_response
    )


@shared_task
def send_mobile_charge_to_topup(charge_id):
    try:
        charge = ChargeLog.objects.get(pk=charge_id)
    except ChargeLog.DoesNotExist:
        return False

    status, msg = send_charge_to_topup(charge)

    charge.status = ChargeLog.CHARGE_STATUS_SUCCESSFUL if status else ChargeLog.CHARGE_STATUS_UNSUCCESSFUL
    charge.log = msg
    charge.save(update_fields=['status', 'log'])

    return True
