# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings
import django.core.validators
import users.models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, blank=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(verbose_name='superuser status', default=False, help_text='Designates that this user has all permissions without explicitly assigning them.')),
                ('username', models.CharField(unique=True, error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 30 characters or fewer starting with a letter. Letters, digits and underscore only.', max_length=32, verbose_name='username', validators=[django.core.validators.RegexValidator('^[a-zA-Z][a-zA-Z0-9_\\.]+$', 'Enter a valid username starting with a-z. This value may contain only letters, numbers and underscore characters.', 'invalid')])),
                ('first_name', models.CharField(max_length=30, blank=True, verbose_name='first name')),
                ('last_name', models.CharField(max_length=30, blank=True, verbose_name='last name')),
                ('email', models.EmailField(null=True, unique=True, max_length=254, blank=True, verbose_name='email address')),
                ('phone_number', models.BigIntegerField(error_messages={'unique': 'A user with this mobile number already exists.'}, blank=True, unique=True, null=True, verbose_name='mobile number', validators=[django.core.validators.RegexValidator('^989[0-3,9]\\d{8}$', 'Enter a valid mobile number.', 'invalid')])),
                ('is_staff', models.BooleanField(verbose_name='staff status', default=False, help_text='Designates whether the user can log into this admin site.')),
                ('is_active', models.BooleanField(verbose_name='active', default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('verify_codes', models.CommaSeparatedIntegerField(max_length=30, blank=True, verbose_name='verification codes')),
                ('groups', models.ManyToManyField(to='auth.Group', related_query_name='user', blank=True, related_name='user_set', help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(to='auth.Permission', related_query_name='user', blank=True, related_name='user_set', help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'user',
                'db_table': 'auth_users',
                'verbose_name_plural': 'users',
            },
            managers=[
                ('objects', users.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('one_signal_id', models.UUIDField(unique=True, verbose_name='one signal ID')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='devices')),
            ],
            options={
                'verbose_name': 'device',
                'db_table': 'auth_devices',
                'verbose_name_plural': 'devices',
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('nick_name', models.CharField(max_length=150, blank=True, verbose_name='nick_name')),
                ('avatar', models.ImageField(blank=True, verbose_name='avatar', upload_to='')),
                ('birthday', models.DateField(null=True, blank=True, verbose_name='birthday')),
                ('gender', models.NullBooleanField(verbose_name='gender', help_text='female is False, male is True, null is unset')),
                ('email', models.EmailField(max_length=254, blank=True, verbose_name='email address')),
                ('phone_number', models.BigIntegerField(null=True, blank=True, verbose_name='mobile number', validators=[django.core.validators.RegexValidator('^989[0-3,9]\\d{8}$', 'Enter a valid mobile number.', 'invalid')])),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'profile',
                'db_table': 'auth_profiles',
                'verbose_name_plural': 'profiles',
            },
        ),
    ]
