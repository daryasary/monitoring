from importlib import import_module

from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.conf import settings

from oauth2client import client

from .models import Device


User = get_user_model()
auth_user_settings = getattr(settings, 'AUTH_USER', {})


def migrate_user(one_signal_id, user):
    if auth_user_settings.get('ONE_SIGNAL_USER'):
        try:
            fake_user = User.objects.get(username=one_signal_id.hex)
        except (User.DoesNotExist, ValueError) as e:
            pass
        else:
            move_func_str = auth_user_settings.get('ONE_SIGNAL_MIGRATE_FUNC')
            if move_func_str:
                try:
                    parts = move_func_str.split('.')
                    module_path, class_name = '.'.join(parts[:-1]), parts[-1]
                    module = import_module(module_path)
                    move_user_func = getattr(module, class_name)
                    move_user_func(user, fake_user)
                except Exception as e:
                    print('User Migrate Error: ', e)

    Device.objects.update_or_create(one_signal_id=one_signal_id, defaults={'user': user})


class SMSBackend(ModelBackend):
    """
    Authenticates against settings.AUTH_USER_MODEL.
    """

    def authenticate(self, username=None, password=None, **kwargs):
        PhoneNumberField = User._meta.get_field('phone_number')
        try:
            phone_number = int(username)
            PhoneNumberField.run_validators(phone_number)
            user = User._default_manager.get_by_phone_number(phone_number)
            if password in user.get_verify_codes():
                one_signal_id = kwargs.pop('one_signal_id', None)
                if one_signal_id:
                    migrate_user(one_signal_id, user)
                return user
        except User.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a non-existing user (#20760).
            User().set_password(password)
        except (ValueError, ValidationError):
            pass


class GoogleOAuthBackend(ModelBackend):

    def authenticate(self, username=None, password=None, **kwargs):
        try:
            token_info = client.verify_id_token(password, None)
        except Exception as e:
            # print('Google Verify Exception: ', e)
            return

        if username != token_info['email']:
            return

        try:
            user = User.objects.get(email=token_info['email'])
        except User.DoesNotExist:
            user = User.objects.create_user(
                email=token_info['email'],
                first_name=token_info.get('given_name', ''),
                last_name=token_info.get('family_name', ''),
            )
        except Exception as e:
            print('Google User Login Error: ', e)
            return

        one_signal_id = kwargs.pop('one_signal_id', None)
        if one_signal_id:
            migrate_user(one_signal_id, user)

        return user
