from django.conf import settings
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from rest_framework import status
from rest_framework.authentication import BaseAuthentication, get_authorization_header

from lib.response_center import CustomApiException
from users.models import Device

User = settings.AUTH_USER_MODEL

"""
    list of auth error codes:

    "1000" :    system validation or custom errors
    "1001" :    invalid header token
    "1002" :    token with bad structure or space
    "1003" :    token expired
    "1004" :    token not exist
    "1005" :    token user is inactive
"""


class TokenAuthentication(BaseAuthentication):
    """
    This authentication scheme uses Knox AuthTokens for authentication.

    Similar to DRF's TokenAuthentication, it overrides a large amount of that
    authentication scheme to cope with the fact that Tokens are not stored
    in plaintext in the database

    If successful
    - `request.user` will be a django `User` instance
    - `request.auth` will be an `AuthToken` instance
    """
    model = Device

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != b'token':
            return None

        if len(auth) == 1:
            msg = _('Invalid token header. No credentials provided.')
            raise CustomApiException(False, msg, "1001", status.HTTP_401_UNAUTHORIZED)
        elif len(auth) > 2:
            msg = _('Invalid token header. Token string should not contain spaces.')
            raise CustomApiException(False, msg, "1002", status.HTTP_401_UNAUTHORIZED)

        return self.authenticate_credentials(auth[1])

    def authenticate_credentials(self, token):
        """
        Due to the random nature of hashing a salted value, this must inspect
        each auth_token individually to find the correct one.

        Tokens that have expired will be deleted and skipped
        """

        try:
            auth_token = Device.objects.get(token=token)
            if auth_token.expires < timezone.now():
                raise CustomApiException(False, _('token expired'), "1003", status.HTTP_401_UNAUTHORIZED)
            return self.validate_user(auth_token)
        except Device.DoesNotExist:
            raise CustomApiException(False, _("token not match"), "1004", status.HTTP_401_UNAUTHORIZED)

    def validate_user(self, auth_token):
        if not auth_token.user.is_active:
            raise CustomApiException(False, _('user inactive'), "1005", status.HTTP_401_UNAUTHORIZED)

        return auth_token.user, auth_token

    def authenticate_header(self, request):
        return 'Token'
