from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend


class SMSBackend(ModelBackend):
    """
    Authenticates against settings.AUTH_USER_MODEL.
    """

    def authenticate(self, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        PhoneNumberField = UserModel._meta.get_field('phone_number')
        try:
            phone_number = int(username)
            PhoneNumberField.run_validators(phone_number)
            user = UserModel._default_manager.get_by_phone_number(phone_number)
            if password in user.get_verify_codes():
                return user
        except UserModel.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a non-existing user (#20760).
            UserModel().set_password(password)
        except (ValueError, ValidationError):
            pass
