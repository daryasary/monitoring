from django.utils.translation import ugettext_lazy as _

app_messages = {

    "send_verification_error": _("an error occurred in sending verification code to your phone number."),
    "sub_phone_ok": _("verification code is sent to you. please verify your phone to use our system."),
    "no_user": _("no user related to your phone number. please register again."),
    "no_device": _("no device related to your user found."),
    "vas_verify_fail": _("verification of your phone get failed."),
    "verify_viaphone_success": _("your phone number verify successfully."),
    "device_logout": _("your device logged out successfully."),

    "refresh_token_success": _("your token refreshed successfully."),
    "user_exist": _("user with this information exist in our database."),
    "password_changed": _("yor account password changed successfully."),

    "pass_change_failed": _("fail in changing password."),
    "login_success": _("you logged in successfully."),
    "register_mail_success": _("you registered by mail successfully."),

    "fail_update_profile": _("an error occurred when we try to update your profile."),
    "google_token_problem": _("there is a problem in your google token."),
    "google_problem": _("login or register by google auth2 got failed"),

    "login_google_success": _("you logged in now using google auth 2."),

}
