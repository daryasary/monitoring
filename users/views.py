import requests
from random import randint

from django.template.loader import render_to_string
from django.contrib.auth import get_user_model

from rest_framework import generics
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from .drf_jwt import ObtainJSONWebToken
from .serializers import ProfileSerializer, RegisterSerializer
from .models import UserProfile


User = get_user_model()
obtain_jwt_token = ObtainJSONWebToken.as_view()


def send_msg(phone_number, message):
    query_params = {
        'mobileno': phone_number,
        'body': message,
        'SecretValue': "x54sdSdTTf6gjDSdfj",
    }

    try:
        r = requests.get("http://91.99.103.210/api/adp/send", params=query_params, timeout=10)
    except Exception as e:
        print(e)
        return False
    else:
        if r.status_code // 100 != 2:
            return False

    return True


def send_verification():
    pass


class ProfileAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = ProfileSerializer
    queryset = UserProfile.objects.select_related('user').all()
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        user = self.request.user
        obj, created = UserProfile.objects.select_related('user').get_or_create(
            user=user,
            defaults={
                'nick_name': user.first_name,
                'phone_number': user.phone_number,
                'email': user.email or '',
            }
        )
        return obj

    def perform_update(self, serializer):
        instance = serializer.save()
        if instance.phone_number != instance.user.phone_number:
            send_verification()


class RegisterAPIView(generics.CreateAPIView):
    serializer_class = RegisterSerializer
    queryset = User.objects.all()

    def perform_create(self, serializer):
        # verify_code = "11111"
        verify_code = str(randint(10000, 99999))
        serializer.save(verify_code=verify_code)
        msg = render_to_string(
            'phone_verify.text',
            context={
                'verify_code': verify_code,
            },
        )
        sent_message = send_msg(serializer.data['phone_number'], msg)
        if not sent_message:
            raise ValidationError({'detail': 'Could not sent verification SMS'})
