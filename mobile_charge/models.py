from khayyam import JalaliDatetime, JalaliDate

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


class PackageLiveManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_enable=True)


class Package(models.Model):
    created_time = models.DateTimeField(_('creation on'), auto_now_add=True)
    updated_time = models.DateTimeField(_('modified on'), auto_now=True)
    title = models.CharField(_('title'), max_length=150)
    price = models.PositiveIntegerField(_('price'))
    topup_package = models.UUIDField(_('topup package UUID'))
    is_enable = models.BooleanField(_('is enable'), default=True)

    objects = models.Manager()
    live = PackageLiveManager()

    class Meta:
        db_table = 'packages'
        verbose_name = _('package')
        verbose_name_plural = _('packages')
        ordering = ('title', 'id')

    def __str__(self):
        return self.title


class UserChargeLimit(models.Model):
    created_time = models.DateTimeField(_('creation on'), auto_now_add=True)
    updated_time = models.DateTimeField(_('modified on'), auto_now=True)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=_('user'), related_name='user_charge_limit')
    charge_limit = models.PositiveIntegerField(_('charge limit per day'))

    class Meta:
        db_table = 'user_charge_limit'
        verbose_name = _('user charge limit')
        verbose_name_plural = _('user charge limits')


class ChargeLog(models.Model):
    CHARGE_STATUS_WAITING = 0
    CHARGE_STATUS_SUCCESSFUL = 1
    CHARGE_STATUS_UNSUCCESSFUL = 2
    CHARGE_STATUS_CHOICES = (
        (CHARGE_STATUS_WAITING, _('waiting')),
        (CHARGE_STATUS_SUCCESSFUL, _('successful')),
        (CHARGE_STATUS_UNSUCCESSFUL, _('unsuccessful'))
    )

    created_time = models.DateTimeField(_('creation on'), auto_now_add=True, db_index=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('user'), related_name='charge_logs', on_delete=models.PROTECT)
    package = models.ForeignKey(Package, verbose_name=_('package'), related_name='charge_logs', on_delete=models.PROTECT)
    phone_number = models.BigIntegerField(_('phone number'), db_index=True)
    price = models.PositiveIntegerField(_('price'))
    status = models.PositiveSmallIntegerField(_('status'), choices=CHARGE_STATUS_CHOICES)
    log = models.TextField(_('charge log'), blank=True)

    class Meta:
        db_table = 'charge_logs'
        verbose_name = 'charge log'
        verbose_name_plural = 'charge logs'

    @property
    def jalali_charged_time(self):
        return JalaliDatetime(self.created_time).strftime('%Y-%m-%d %H:%M')
    jalali_charged_time.fget.short_description = _('charged time')

    @property
    def jalali_charged_date(self):
        return JalaliDate(self.created_time).strftime('%Y-%m-%d')
    jalali_charged_date.fget.short_description = _('charged date')
