from django.contrib import admin

from .models import Package, UserChargeLimit, ChargeLog


@admin.register(Package)
class PackageAdmin(admin.ModelAdmin):
    list_display = ('title', 'price', 'is_enable', 'topup_package')
    list_filter = ('is_enable', 'price')


@admin.register(UserChargeLimit)
class UserChargeLimitAdmin(admin.ModelAdmin):
    list_display = ('user', 'charge_limit')


@admin.register(ChargeLog)
class ChargeLogAdmin(admin.ModelAdmin):
    list_display = ('created_time', 'user', 'package', 'phone_number', 'price', 'status')
    readonly_fields = ('user', 'package', 'phone_number', 'price', 'status', 'log')
    list_filter = ('status', 'user', 'package')
    search_fields = ('phone_number', )
    date_hierarchy = 'created_time'
    actions = None

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
