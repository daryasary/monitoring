# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('mobile_charge', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chargelog',
            name='status',
            field=models.PositiveSmallIntegerField(verbose_name='status', choices=[(0, 'waiting'), (1, 'successful'), (2, 'unsuccessful')]),
        ),
        migrations.AlterField(
            model_name='userchargelimit',
            name='user',
            field=models.OneToOneField(verbose_name='user', related_name='user_charge_limit', to=settings.AUTH_USER_MODEL),
        ),
    ]
