# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ChargeLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_time', models.DateTimeField(verbose_name='creation on', auto_now_add=True, db_index=True)),
                ('phone_number', models.BigIntegerField(verbose_name='phone number', db_index=True)),
                ('price', models.PositiveIntegerField(verbose_name='price')),
                ('status', models.PositiveSmallIntegerField(verbose_name='status', choices=[(1, 'successful'), (2, 'unsuccessful')])),
                ('log', models.TextField(verbose_name='charge log', blank=True)),
            ],
            options={
                'verbose_name': 'charge log',
                'verbose_name_plural': 'charge logs',
                'db_table': 'charge_logs',
            },
        ),
        migrations.CreateModel(
            name='Package',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_time', models.DateTimeField(verbose_name='creation on', auto_now_add=True)),
                ('updated_time', models.DateTimeField(verbose_name='modified on', auto_now=True)),
                ('title', models.CharField(verbose_name='title', max_length=150)),
                ('price', models.PositiveIntegerField(verbose_name='price')),
                ('topup_package', models.UUIDField(verbose_name='topup package UUID')),
                ('is_enable', models.BooleanField(verbose_name='is enable', default=True)),
            ],
            options={
                'verbose_name': 'package',
                'verbose_name_plural': 'packages',
                'ordering': ('title', 'id'),
                'db_table': 'packages',
            },
        ),
        migrations.CreateModel(
            name='UserChargeLimit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_time', models.DateTimeField(verbose_name='creation on', auto_now_add=True)),
                ('updated_time', models.DateTimeField(verbose_name='modified on', auto_now=True)),
                ('charge_limit', models.PositiveIntegerField(verbose_name='charge limit per day')),
                ('user', models.OneToOneField(verbose_name='user', related_name='charge_limit', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'user charge limit',
                'verbose_name_plural': 'user charge limits',
                'db_table': 'user_charge_limit',
            },
        ),
        migrations.AddField(
            model_name='chargelog',
            name='package',
            field=models.ForeignKey(verbose_name='package', on_delete=django.db.models.deletion.PROTECT, to='mobile_charge.Package', related_name='charge_logs'),
        ),
        migrations.AddField(
            model_name='chargelog',
            name='user',
            field=models.ForeignKey(verbose_name='user', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, related_name='charge_logs'),
        ),
    ]
