import os


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
LOG_DIR = os.path.join(BASE_DIR, 'logs')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '[%(asctime)s] %(levelname)s %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
    },

    'handlers': {
        'django-requests': {
            'level': 'WARNING',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'django.log'),
            'formatter': 'verbose'
        },
        'login-logout': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'login-logout.log'),
            'formatter': 'simple'
        },
        'reports-requests': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'reports-requests.log'),
            'formatter': 'simple'
        },
        'reports-tasks': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'reports-tasks.log'),
            'formatter': 'simple'
        },
        'reports-add-remove': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'reports-add-remove.log'),
            'formatter': 'simple'
        },
        # 'vas-payment': {
        #     'level': 'INFO',
        #     'class': 'logging.FileHandler',
        #     'filename': os.path.join(LOG_DIR, 'vas-payment.log'),
        #     'formatter': 'simple'
        # },
        'saman-bank': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'saman-bank.log'),
            'formatter': 'simple'
        },
        'save-reports-mongo': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'save-reports-to-mongo.log'),
            'formatter': 'simple'
        },
        'send-sms': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'send-sms.log'),
            'formatter': 'simple'
        },
        'mobile-charge': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'mobile-charge.log'),
            'formatter': 'simple'
        }
    },

    'loggers': {
        'django.request': {
            'handlers': ['django-requests', ],
            'level': 'WARNING',
            'propagate': False,
        },
        'login_logout': {
            'handlers': ['login-logout', ],
            'level': 'INFO',
            'propagate': False,
        },
        'reports_requests': {
            'handlers': ['reports-requests', ],
            'level': 'INFO',
            'propagate': False
        },
        'reports_add_remove': {
            'handlers': ['reports-add-remove', ],
            'level': 'INFO',
            'propagate': False
        },
        'save_reports_mongo': {
            'handlers': ['save-reports-mongo', ],
            'level': 'INFO',
            'propagate': False
        },
        'reports_tasks': {
            'handlers': ['reports-tasks', ],
            'level': 'INFO',
            'propagate': False
        },
        # 'vas_payment': {
        #     'handlers': ['vas-payment', ],
        #     'level': 'INFO',
        #     'propagate': False
        # },
        'saman_bank': {
            'handlers': ['saman-bank', ],
            'level': 'INFO',
            'propagate': False
        },
        'sms_send': {
            'handlers': ['send-sms', ],
            'level': 'INFO',
            'propagate': False
        },
        'mobile_charge': {
            'handlers': ['mobile-charge', ],
            'level': 'INFO',
            'propagate': False
        }
    },
}
