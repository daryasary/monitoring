from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = [

    # Services and Reports
    url(r'^api/services/', include('service.urls')),

    # Internal Reports
    url(r'^api/reports/', include('reports.urls')),

    # Admin Panel
    url(r'^api/admin/', include(admin.site.urls)),

    # Bank Reports
    # url(r'^api/bank_reports/', include('bank_reports.urls')),

]
